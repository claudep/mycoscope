from django.contrib import admin
from django.contrib.auth.models import User
from django.urls import path
from proj.models import BCISUser


class BCISUserAdmin(admin.ModelAdmin):
    def get_urls(self):
        # Redirect /password to User admin user_change_password view
        return [
            path('<int>/password/', self.admin_site.admin_view(self.admin_site._registry[User].user_change_password))
        ] + super().get_urls()


admin.site.register(BCISUser, BCISUserAdmin)
