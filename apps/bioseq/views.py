# Copyright (c) 2010 Claude Paroz <claude@2xlibre.net>

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models.signals import post_save
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils.translation import gettext as _
from django.views.decorators.http import require_POST

from invent.models import BaseObject
from invent.views import permission_denied
from . import models
from .forms import BioEntryForm, ImportForm
from .utils import import_sequence, make_blast_db


def import_seq_file(request, obj_id):
    """ Import a sequence file """
    obj = get_object_or_404(BaseObject, pk=obj_id)
    if not obj.can_edit(request.user):
        return HttpResponseForbidden(_("You are not allowed to edit this object"))
    import_form = ImportForm(request.POST or None, request.FILES or None)
    if request.method == 'POST':
        if import_form.is_valid():
            seqfile = import_form.cleaned_data.get('seqfile', None)
            acc_num = import_form.cleaned_data.get('ncbi_accession', None)
            target = import_form.cleaned_data['target']
            bioentries = []
            # Disconnecting signal so make_blast_db runs only at the end of all imports
            post_save.disconnect(models.update_blast_db, sender=models.BioSequence)
            try:
                bioentries = import_sequence(obj, seqfile, acc_num, target=target)
            except Exception as e:
                messages.error(request, str(e))
            else:
                messages.success(request, "%d sequences have been updated." % len(bioentries))
                if len(bioentries) == 1:
                    return HttpResponseRedirect(reverse('display_sequence', args=[obj.pk, bioentries[0].pk]))
                else:
                    return HttpResponseRedirect(reverse('object_detail', args=[obj.pk]))
            finally:
                post_save.connect(models.update_blast_db, sender=models.BioSequence)
                if len(bioentries) > 0:
                    make_blast_db()
    context = {
        'obj'   : obj,
        'form'  : import_form,
    }
    return render(request, 'bioseq/import.html', context)


def display(request, obj_id, seq_id):
    """ Display a sequence """
    obj = get_object_or_404(BaseObject, pk=obj_id)
    bioentry = get_object_or_404(models.BioEntry, pk=seq_id)
    format = request.GET.get('format', 'html')
    if not obj.can_view(request.user):
        return permission_denied(request)
    breadcrumb = obj.breadcrumb(link_self=True)
    breadcrumb.append((_("Molecular sequence"), ""))
    context = {
        'obj'     : obj,
        'breadcrumb' : breadcrumb,
        'editable': obj.can_edit(request.user),
        'bioentry': bioentry,
    }
    if format == 'html':
        template = 'bioseq/display.html'
        context['sequence'] = bioentry.get_sequence()
        context['features'] = bioentry.seqfeature_set.all().order_by('rank')
    else:
        if format == 'genbank' and bioentry.biosequence.alphabet == 'unknown':
            context['raw'] = """Sorry, not enough information for this sequence to be displayed in GenBank format"""
        else:
            context['raw'] = bioentry.format(format)
        template = 'bioseq/display-formatted.html'
    return render(request, template, context)


def edit_sequence(request, obj_id, seq_id):
    obj = get_object_or_404(BaseObject, pk=obj_id)
    bioentry = get_object_or_404(models.BioEntry, pk=seq_id)
    if request.method == 'POST':
        form = BioEntryForm(instance=bioentry, data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("display_sequence", args=[obj.pk, bioentry.pk]))
    else:
        form = BioEntryForm(instance=bioentry)
    context = {
        'object': obj,
        'form': form,
    }
    return render(request, 'bioseq/bioentry_edit.html', context)


@require_POST
def delete_sequence(request, obj_id, seq_id):
    """ Delete a sequence """
    # Get sequence, check rights, delete, redirect to object details
    obj = get_object_or_404(BaseObject, pk=obj_id)
    bioentry = get_object_or_404(models.BioEntry, pk=seq_id)
    if not obj.can_edit(request.user):
        return HttpResponseForbidden(_("You are not allowed to edit this object"))
    if bioentry.baseobjects.count() > 1:
        bioentry.baseobjects.remove(obj)
    else:
        bioentry.delete()
    return HttpResponseRedirect(reverse('object_detail', args=[obj.pk]))

@login_required
def blast_sequence(request, obj_id, seq_id, mode):
    """ Run a local blast with sequence data """
    obj = get_object_or_404(BaseObject, pk=obj_id)
    bioentry = get_object_or_404(models.BioEntry, pk=seq_id)
    blast = bioentry.biosequence.blastn(request.user, mode)
    return HttpResponseRedirect(reverse('blast_display', args=[obj.pk, blast.pk]))

def blast_display(request, obj_id, blast_id):
    """ Display a specific blast result """
    obj = get_object_or_404(BaseObject, pk=obj_id)
    blast = get_object_or_404(models.BlastResult, pk=blast_id)
    record = blast.read()
    breadcrumb = obj.breadcrumb(link_self=True)
    breadcrumb.append((blast.bioentry.name, reverse("display_sequence", args=[obj.pk, blast.bioentry.pk])))
    breadcrumb.append((_("Blast result"), ""))
    context = {
        'obj'   : obj,
        'breadcrumb' : breadcrumb,
        'blast' : blast,
        'record': record,
    }
    return render(request, 'bioseq/blast_result.html', context)
