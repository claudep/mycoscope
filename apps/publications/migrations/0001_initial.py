from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="PressCorner",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "publishing_datetime",
                    models.DateTimeField(
                        blank=True, null=True, verbose_name="publishing datetime"
                    ),
                ),
                ("title", models.CharField(verbose_name="title")),
                ("summary", models.TextField(blank=True, verbose_name="summary")),
                ("link", models.URLField(blank=True, verbose_name="link")),
                (
                    "pdf_file",
                    models.FileField(
                        blank=True,
                        upload_to="publications/press_corner/",
                        verbose_name="pdf file",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.CreateModel(
            name="Publication",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "publishing_datetime",
                    models.DateTimeField(
                        blank=True, null=True, verbose_name="publishing datetime"
                    ),
                ),
                ("title", models.CharField(verbose_name="title")),
                ("summary", models.TextField(blank=True, verbose_name="summary")),
                ("link", models.URLField(blank=True, verbose_name="link")),
                (
                    "pdf_file",
                    models.FileField(
                        blank=True,
                        upload_to="publications/publications/",
                        verbose_name="pdf file",
                    ),
                ),
                ("author", models.CharField(blank=True, verbose_name="author")),
            ],
            options={
                "abstract": False,
            },
        ),
    ]
