from datetime import timedelta
from tempfile import NamedTemporaryFile

from django.http import HttpResponse

from openpyxl import Workbook
from openpyxl.styles import Font
from openpyxl.utils import get_column_letter

openxml_contenttype = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'


class OpenXMLExport:
    def __init__(self, sheet_title):
        self.wb = Workbook()
        self.ws = self.wb.active
        self.ws.title = sheet_title
        self.bold = Font(name='Calibri', bold=True)
        self.row_idx = 1

    def write_line(self, values, bold=False, col_widths=()):
        for col_idx, value in enumerate(values, start=1):
            cell = self.ws.cell(row=self.row_idx, column=col_idx)
            if isinstance(value, timedelta):
                cell.number_format = '[h]:mm;@'
            try:
                cell.value = value
            except KeyError:
                # Ugly workaround for https://bugs.python.org/issue28969
                from openpyxl.utils.datetime import to_excel
                to_excel.cache_clear()
                cell.value = value
            if bold:
                cell.font = self.bold
            if col_widths:
                self.ws.column_dimensions[get_column_letter(col_idx)].width = col_widths[col_idx - 1]
        self.row_idx += 1

    def add_sheet(self, title):
        self.wb.create_sheet(title)
        self.ws = self.wb[title]
        self.row_idx = 1

    def get_http_response(self, filename):
        with NamedTemporaryFile() as tmp:
            self.wb.save(tmp.name)
            tmp.seek(0)
            response = HttpResponse(tmp, content_type=openxml_contenttype)
            response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
        return response
