# Examples:
#   $ fab [-H www.bcis.ch] clone-remote-pgdb hortus

import getpass
from fabric import task
from invoke import Context, Exit

MAIN_HOST = 'agroscope'
VIRTUALENV_DIR = '/var/virtualenvs/mycoscope/bin/activate'

@task(hosts=[MAIN_HOST])
def clone_remote_db(conn, dbname='mycoscope'):
    """ Dump a remote database and load it locally """
    local = Context()

    def exist_local_db(db):
        cmd = local.run('psql --list', hide=True)
        db_list = [line.split()[0] for line in cmd.stdout.split('\n')[3:-3]]
        return db in db_list

    def exist_username(db):
        cmd = local.run('psql -d postgres -c "select usename from pg_user;"', hide=True)
        user_list = [line.strip() for line in cmd.stdout.split('\n')][2:-3]
        return db in user_list

    conn.config['sudo']['password'] = getpass.getpass("Enter the sudo password (on the server):")
    conn.run(f'touch {dbname}.backup && chmod o+rw {dbname}.backup')
    conn.sudo(f'pg_dump --no-owner -Fc -b -f "{dbname}.backup" {dbname}', user='postgres')
    conn.get(f'{dbname}.backup')

    if exist_local_db(dbname):
        rep = input(f'A local database named "{dbname}" already exists. Overwrite? (y/n)')
        if rep == 'y':
            local.run(f'psql -d postgres -c "DROP DATABASE {dbname};"')
        else:
            raise Exit("Database not copied")

    if exist_username(dbname):
        owner = dbname
    else:
        owner = "claude"
    local.run(f'psql -d postgres -c "CREATE DATABASE {dbname} OWNER={owner};"')
    local.run(f'pg_restore -U {owner} -d {dbname} --no-owner "{dbname}.backup"')


# ex: fab [-H www.mycoscope.ch] set-server-maintenance on
@task(hosts=[MAIN_HOST])
def set_server_maintenance(conn, switch):
    """ Pass the server into or out of maintenance mode
        switch is "on" or "off" """
    with conn.cd('/var/www/mycoscope'):
        if switch == "on":
            conn.run('sed -i -e "s/UPGRADING = False/UPGRADING = True/" common/wsgi.py')
        elif switch == "off":
            conn.run('sed -i -e "s/UPGRADING = True/UPGRADING = False/" common/wsgi.py')
        else:
            raise Exit("The parameter should be either on or off")
        conn.run('touch common/wsgi.py')


# ex: fab deploy --project=proj_name
@task(hosts=[MAIN_HOST])
def deploy(conn):
    """ Apply code change to all projects """
    set_server_maintenance(conn, 'on')
    proj_name = 'mycoscope'
    with conn.cd('/var/www/%s' % proj_name):
        conn.run('git stash && git pull && git stash pop')
        with conn.prefix('source %s' % VIRTUALENV_DIR):
            conn.run('python manage.py migrate')
            conn.run('python manage.py collectstatic --noinput')
            conn.run('python manage.py compilemessages')
    set_server_maintenance(conn, 'off')
