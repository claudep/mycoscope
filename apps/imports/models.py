import logging
from collections import OrderedDict
from time import time

from openpyxl import load_workbook
import ooolib  # package ooolib-python

from django.db import models, transaction
from django.core.files import File
from django.utils.translation import gettext as _
from django.contrib.auth.models import User
from invent.models import BaseObject, ObjectType, Attribute, ObjectValue, ObjectValueFactory

class UnsupportedFileFormat(Exception):
    pass

class SavedFile(models.Model):
    """ This model class is used to archive imported files """
    ifile       = models.FileField(upload_to='imported', verbose_name=_("File"))
    stamp       = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.TextField()

    def __str__(self):
        return "%s uploaded by %s the %s" % (self.ifile, self.user, self.stamp)

    def save(self, tempfile, **kwargs):
        """ Save tempfile (an ImportedFile instance) """
        with open(tempfile.file_path, mode='rb') as fh:
            self.ifile.save(tempfile.orig_name, File(fh), False)
        super().save(**kwargs)


class ImportedFile(object):
    """ Class to abstract an imported file, support xls and ods files """
    def __init__(self, datafile, sheet_index=0, sheet_name=None, header_line=1, data_first_line=2):
        """ datafile may be a file path or a django (Uploaded)File object """
        self.header_line = header_line
        self.data_first_line = data_first_line
        if isinstance(datafile, str):
            self.file_path = datafile
            self.orig_name = datafile.rsplit("/", 1)[1]
        else:
            self.file_path = datafile.temporary_file_path()
            self.orig_name = datafile.name
        self.format = self._sniff_format(datafile)
        if self.format in ("xls", "xlsx"):
            self.book = load_workbook(self.file_path, read_only=True)
            if sheet_name:
                self.data_sheet = self.book[sheet_name]
            else:
                self.data_sheet = self.book.active
            self.nrows = self.data_sheet.max_row
            self.ncols = self.data_sheet.max_column
        elif self.format == "ods":
            self.book = ooolib.Calc(opendoc=self.file_path)
            self.book.set_sheet_index(sheet_index)
            self.data_sheet = self.book
            (self.ncols, self.nrows) = self.data_sheet.get_sheet_dimensions()
        else:
            raise UnsupportedFileFormat("Unknown file extension '%s'" % self.format)
        self._headers = []

    @classmethod
    def _sniff_format(cls, dfile):
        """ dfile may be a file path or a django (Uploaded)File object """
        if isinstance(dfile, str):
            format = dfile.rsplit(".", 1)[1]
        else:
            if "opendocument.spreadsheet" in dfile.content_type:
                format = "ods"
            elif "excel" in dfile.content_type or 'spreadsheetml' in dfile.content_type:
                format = "xls"
            else:
                raise UnsupportedFileFormat("Unknown file type '%s'" % dfile.content_type)
        return format

    def get_headers(self):
        """ Extract headers for the file (according to self.header_line) """
        if not self._headers:
            if self.format in ("xls", "xlsx"):
                row = self.data_sheet[self.header_line]
                for i, cell in enumerate(row):
                    h_value = cell.value.strip() if cell.value else cell.value
                    if h_value:
                        self._headers.append(h_value)
                    else:
                        logging.warning("Empty header in %s" % self.file_path)
                        self._headers.append("--empty--")
            if self.format == "ods":
                for i in range(self.ncols):
                    cell_value = self.data_sheet.get_cell_value(i+1, self.header_line)
                    if cell_value:
                        self._headers.append(self.data_sheet.get_cell_value(i+1, self.header_line)[1].strip())
                    else:
                        logging.warning("Empty header in %s" % self.file_path)
                        self._headers.append("--empty--")
        return self._headers

    def get_row_dict(self, idx):
        row_dict = OrderedDict()
        if not self._headers:
            self._headers = self.get_headers()
        if self.format in ("xls", "xlsx"):
            row = self.data_sheet[idx + 1]
            for i, cell in enumerate(row):
                if type(cell.value) == float and str(cell.value).endswith('.0'):
                    row_dict[self._headers[i]] = int(cell.value)
                else:
                    row_dict[self._headers[i]] = cell.value

        if self.format == "ods":
            for i in range(self.ncols):
                cell_value = self.data_sheet.get_cell_value(i+1, idx+1)
                if cell_value and cell_value[0] == 'formula' and cell_value[1]:
                    raise ValueError(_("The ODS file contains formula. Please convert them to raw values before importing the file."))
                row_dict[self._headers[i]] = cell_value and cell_value[1].replace("<text:line-break/>", "\n")
        return row_dict

def check_value(attr, value):
    """ Check if value is valid for attr, and check if multiple values
        Alway return a list """
    if attr.datatype == "integer":
        try:
            v = int(value)
            return [v]
        except:
            # Try to separate value in multiple integer values
            for sep in [',', '/', ';', '|', u'¦']:
                if sep in value:
                    vals = value.split(sep)
                    try:
                        # Transform "2006, 2007" to [2006, 2007]
                        vals = [int(v.strip()) for v in value.split(sep)]
                        return vals
                    except:
                        raise ValueError("Unable to transform '%s' to a list of integers")
    return [value]


# atomic slow down imports, especially for >500 lines
@transaction.atomic
def import_file(datafile, object_type, user, parent_obj, strict_attr=False, update=False, header_line=1, data_first_line=2):
    """ Create objects from the lines of a data file """
    def is_remark_field(attr_name):
        return attr_name.endswith("_REMARK") and attr_name not in valid_attrs and attr_name.rsplit('_', 1)[0] in valid_attrs

    imp_file = ImportedFile(datafile, header_line=header_line, data_first_line=data_first_line)
    all_attributes = {attr.name: attr for attr in Attribute.objects.filter(active=True)}
    headers = imp_file.get_headers()
    content_type = ObjectType.objects.get(id=int(object_type))
    taxo_attr = [d.name for d in Attribute.objects.filter(group__category='taxo')]
    if strict_attr:
        valid_attrs = [attr.name for attr in content_type.get_attributes()]
        valid_attrs.extend(taxo_attr)
    else:
        # Accept all attributes listed in the database
        valid_attrs = Attribute.all_attr_names()
    imported = 0
    # Check header validity
    errors = []
    for d in headers:
        if d.upper() not in valid_attrs and d != "--empty--":
            # Accept "<attr>_remark" as a comment for the corresponding attr value
            if is_remark_field(d):
                continue
            errors.append(_("Attribute '%s' is not valid") % d)
    if errors:
        raise ValueError(", ".join(errors))
    empty_lines = 0
    for rx in range(data_first_line-1, imp_file.nrows):
        start = time()
        data_dict = imp_file.get_row_dict(rx)
        if all(v in (None, '') for v in data_dict.values()):
            # Ignoring completely empty lines
            empty_lines += 1
            #if empty_lines > 10:
            #    return
            continue
        else:
            empty_lines = 0

        # Create object line by line
        taxo_data = {}
        accenumb = str(data_dict.get('ACCENUMB', ""))
        title = (" ".join((str(data_dict.get('ACCENAME', "")), accenumb))).strip()
        if not update:
            newacc = BaseObject(obj_type=content_type, title=title, owner=user, status=parent_obj.status, parent=parent_obj)
            newacc.save()
        else:
            # Try to get object with ACCENUMB as key
            try:
                newacc = ObjectValue.objects.get(value_text=accenumb, obj__parent=parent_obj,  attr__name='ACCENUMB').obj
            except ObjectValue.DoesNotExist:
                logging.error("Unable to find an existing object with ACCENUMB=%s" % accenumb)
                continue
        # Add attributes
        for key, value in data_dict.items():
            if key == '--empty--':
                continue
            if value == "" or value is None:
                if update:
                    try:
                        existing_val = ObjectValue.objects.get(obj=newacc, attr__name=key)
                        existing_val.delete()
                    except ObjectValue.DoesNotExist:
                        pass
                continue
            if key in taxo_attr:
                # Extract taxonomic descriptors (bind with taxon will be tried later)
                taxo_data[key] = data_dict.pop(key)
                continue
            if key == 'DESCRIPTION':
                newacc.description = value
                newacc.save()
                continue
            if is_remark_field(key):
                attr = all_attributes[key.rsplit('_', 1)[0]]
                try:
                    oval = ObjectValue.objects.get(obj=newacc, attr=attr)
                except ObjectValue.DoesNotExist:
                    raise ValueError(_("%(attr)s_REMARK field has a value but there is no corresponding %(attr)s value") % {'attr': attr})
                oval.comments = value
                oval.save()
                continue
            try:
                attr = Attribute.objects.get(name=key)
            except Attribute.DoesNotExist:
                raise ValueError(_("No attribute with name=%s") % key)
            values = check_value(attr, value)
            for v in values:
                newval = None
                if update:
                    try:
                        newval = ObjectValue.objects.get(obj=newacc, attr=attr)
                    except ObjectValue.DoesNotExist:
                        pass
                if not newval:
                    newval = ObjectValueFactory(obj=newacc, attr=attr)
                try:
                    newval.set_value({'value_%s' % key: v})
                except Exception as e:
                    errors.append("Line %(num)d: unable to set value '%(value)s' for field '%(attrib)s' (error was: '%(error)s')" % {
                        'num': rx, 'value': v, 'attrib': key, 'error': str(e) })
                    raise ValueError(errors[0])
        if not update:
            # Updating the taxon link is not supported yet
            try:
                newacc.taxo_bind(taxo_data)
            except ValueError as e:
                raise ValueError(_("Line %(num)d: %(msg)s") % {'num': rx, 'msg': str(e)})
        elapsed = time() - start
        logging.warning("Imported object %d/%d (%f)" % (imported, imp_file.nrows-1, elapsed))
        imported += 1
    # Archive imported file
    saved = SavedFile(user=user, description="user-imported file containing objects (attached to BaseObject %s)" % parent_obj.uuid)
    saved.save(imp_file)

    return _("%(num1)d objects imported on %(num2)d total rows") % {
        'num1':imported, 'num2':imp_file.nrows-(data_first_line-1)}
