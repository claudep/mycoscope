# Copyright (c) 2009-2011 Claude Paroz <claude@2xlibre.net>
import json
import re
from collections import OrderedDict
from pathlib import Path
from random import choice
from urllib.parse import urlencode

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ValidationError
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.db.models import Q
from django.http import (
    HttpResponse, HttpResponseRedirect, HttpResponseForbidden, Http404, JsonResponse,
)
from django.shortcuts import redirect, render, get_object_or_404
from django.template import Template, Context, loader
from django.urls import reverse
from django.utils.translation import gettext as _
from django.views import View
from django.views.decorators.http import require_POST
from django.views.generic import TemplateView, UpdateView

from imports.models import import_file
from permission.forms import PermissionEditForm

from . import forms
from .exports import OpenXMLExport
from .models import (
    BaseObject, ObjectType, ObjectValue, MetaValue, Attribute, AttributeGroup,
    AttributeInGroup, AttributeForObject, PhotoAttachment, FileAttachment
)

HOME_TUPLE = (_("Home"), "/")

class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Randomly choose an image from media dir
        path = Path(settings.MEDIA_ROOT) / 'photos_home'
        random_path = choice(list(path.iterdir()))
        context.update({
            'form': forms.SimpleSearchForm(),
            'img_url': f'{settings.MEDIA_URL}photos_home/{random_path.name}',
            'img_name': random_path.stem.replace('_', ' ').replace(' sp', ' sp.'),
        })
        return context


def autocomplete(request):
    max_items = 8
    q = request.GET.get('q')
    has_more = False
    if q:
        query = forms.SearchForm(data={}, user=request.user)._base_query()
        query = query.filter(title__icontains=q, obj_type__code='specimen').distinct()
        results = list(query.values('pk', 'title', 'status')[:max_items + 1])
        for res in results:
            res['url'] = reverse('object_detail', args=[res['pk']])
        if len(results) > max_items:
            has_more = True
            results.pop()
        #sqs = SearchQuerySet().autocomplete(text_auto=q)
        #results = [str(result.object) for result in sqs[:max_items]]
    else:
        results = []
    return JsonResponse({'results': results, 'has_more': has_more})


def object_detail(request, obj_id):
    obj = get_object_or_404(BaseObject, pk=obj_id)
    if not obj.can_view(request.user):
        return permission_denied(request)
    editable = obj.can_edit(request.user)
    reverse_paginator = Paginator(obj.get_reverse_objects(), 10)
    context = {
        'breadcrumb': obj.breadcrumb(),
        'obj': obj,
        'grouped_values': obj.get_grouped_values(request.user, with_taxo=False),
        'editable': editable,
        'attr_groups': editable and obj.obj_type.get_groups(categ_exclude=['taxo'], with_not_grouped=True),
        'containable_types': obj.obj_type.can_contain.exclude(code='image'),
        'photos':   obj.photos.all(), # old system with generic relation
        'images':   obj.get_images(), # new system with sub object
        'grouped_children': {},
        'reverse_count'  : reverse_paginator.count,
        'reverse_objects': reverse_paginator.page(1),
        'specimen_dead': obj.get_value_for('ACCSTATUS') == 'Dead',
    }
    if obj.is_containable():
        search_text = ""
        context['search_form'] = forms.SimpleSearchForm(required=False, data=request.GET)
        if context['search_form'].is_valid():
            search_text = context['search_form'].cleaned_data["text"]

        children_by_type = obj.get_children_by_type(
            add_filters={"title__icontains": search_text} if search_text else {}
        )
        for obj_type, content in children_by_type.items():
            if obj_type.code == 'image':
                continue # Images as Objects are special-cased
            paginator = Paginator(content[1], 10)
            no_page = int(request.GET.get('p%d' % obj_type.pk, '1'))
            try:
                page = paginator.page(no_page)
            except (EmptyPage, InvalidPage):
                page = paginator.page(paginator.num_pages)
            context['grouped_children'][obj_type] = (content[0], page)
            context['allow_tabular'] = tabular_view_allowed(request.user)
            context['more_url'] = reverse('object_list', kwargs={"obj_id": obj.id, "otype": obj_type.code})
            if search_text:
                params = urlencode({"text": search_text})
                context['more_url'] = f"{context['more_url']}?{params}"
    return render(request, 'invent/object_detail.html', context)

def object_image(request, obj_id):
    """ View of an image object, limited to image and attributes (without other context)
        generally AJAX-called to fill main content area (div id='content_blocks') """
    image = get_object_or_404(BaseObject, pk=obj_id)
    if not image.can_view(request.user):
        return permission_denied(request)
    editable = image.can_edit(request.user)
    context = {
        'image': image,
        'grouped_values': image.get_grouped_values(request.user),
        'editable': editable,
        'img_attrs': editable and image.obj_type.get_attributes(),
    }
    return render(request, 'invent/object_detail_image.html', context)

def permission_denied(request):
    context = {}
    return render(request, 'invent/denied.html', context)

'''def object_list(request, obj_id):
    """ List the content of an object """
    obj = get_object_or_404(BaseObject, pk=obj_id)
    context = {
        'obj': obj,
    }
    return render(request, 'invent/object_list.html', context)
'''

def object_list_table(request, obj_id, otype, format='html'):
    # Not optimal to call by_uuid, TODO: factorize code elsewhere...
    list_obj = get_object_or_404(BaseObject, pk=obj_id)
    return object_list_by_uuid(request, list_obj.uuid, format=(format=='table' and 'html' or format), otype=otype)

def object_list_by_uuid(request, uuid, format='html', otype='all'):
    """ With format == 'html', returns a basic HTML-formatted table suitable for import in spreadsheets """
    list_obj = get_object_or_404(BaseObject, uuid=uuid)
    all_attrs = list_obj.get_children_existing_attrs()
    children = list_obj.get_children().exclude(obj_type__code='image')
    if otype == "all":
        obj_type_num = children.values_list('obj_type').distinct().count()
        if obj_type_num > 1:
            return http_403(request, "Unable to choose which children list to render")
        otype_obj = children[0].obj_type
    else:
        # Sort attributes depending on objecttype attributes
        otype_obj = get_object_or_404(ObjectType, code=otype)

    sorted_attrs = otype_obj.get_attributes()

    def get_index(attr):
        try:
            return sorted_attrs.index(attr)
        except ValueError:
            return 1000

    all_attrs = sorted(all_attrs, key=get_index, reverse=True)
    all_values = [] # List of value dicts
    empty_value = ''
    for obj in children.order_by('title'):
        val_dict, remaining_dict = obj.get_attr_dict(all_attrs, empty_value)
        if remaining_dict:
            # There are attributes not in all_attrs: complete all_attrs and all_values already constructed
            # Due to their special treatment, taxo values may be in remaining_dict, try to
            # order them properly
            taxo_attrs = OrderedDict(AttributeInGroup.objects.filter(group__category='taxo').values_list('attribute__name', 'position').order_by('position'))
            remaining_dict = OrderedDict([(key[1], key[2]) for key in sorted([(taxo_attrs.get(k,0), k, val) for k, val in remaining_dict.items()], reverse=True)])
            remaining_keys = remaining_dict.keys()
            # Extend all_attrs, then re-parse all_values to add missing keys
            all_attrs.extend([Attribute.objects.get(name=name) for name in remaining_keys])
            for val in all_values:
                for k in remaining_keys:
                    val[k] = empty_value
            val_dict.update(remaining_dict)
        all_values.append(val_dict)

    # Reverse all_attrs and dict in all_values, so 'automatic' attrs are first
    all_attrs.reverse()
    all_values = [
        OrderedDict([(key, val[key]) for key in reversed(val.keys())])
        for val in all_values
    ]

    # Add statistics lines
    all_stats_attrs = [aig.attribute for aig in AttributeInGroup.objects.filter(
        group__category='statistics',
        attribute__pk__in=list_obj.objectvaluestats_set.values_list('attr_id', flat=True).distinct()
        ).order_by('position')]
    stats_values = dict(("%s-%s" % (ovs.attr.name, ovs.attr_subj.name), ovs.get_value()) for ovs in list_obj.objectvaluestats_set.all())
    all_stats_values = []
    for stat_attr in all_stats_attrs:
        stats_dict = OrderedDict({'head': stat_attr.atitle})
        for attr in all_attrs:
            stats_dict[attr.name] = stats_values.get("%s-%s" % (stat_attr.name, attr.name), "-")
        all_stats_values.append(stats_dict)

    context = {
        'obj'        : list_obj,
        'headers'    : [a.name for a in all_attrs],
        'sdict_list' : all_values,
        'has_stats'  : bool(all_stats_values),
        'stats_list' : all_stats_values,
        'num_children': len(children),  # query was already resolved.
    }
    if is_ajax(request):
        template = 'invent/object_list_table.html'
    else:
        template = 'invent/object_list_table_container.html'
        context['caption'] = list_obj.get_title()
    return render(request, template, context)


class PaginatedObjectsView(TemplateView):
    """ Called by pagination arrows in object list block """
    template_name = 'invent/object_list_block.html'
    named_url     = 'object_list'
    display_style = 'short'

    @property
    def search_text(self):
        if not self.request.GET.get("text"):
            return ""
        return self.request.GET.get("text").strip()

    def get_queryset(self):
        self.list_obj = get_object_or_404(BaseObject, pk=int(self.kwargs['obj_id']))
        if self.kwargs['otype'] == 'all':
            children = self.list_obj.get_children().all()
        else:
            children = self.list_obj.get_children().filter(obj_type__code=self.kwargs['otype'])

        if self.search_text:
            children = children.filter(title__icontains=self.search_text)

        return children.order_by('title')

    def get_context_data(self, **kwargs):
        paginator = Paginator(self.object_list, 10)
        no_page = int(self.request.GET.get('p', '1'))

        more_url = reverse(self.named_url, kwargs=self.kwargs)
        if self.search_text:
            params = urlencode({"text": self.search_text})
            more_url = f"{more_url}?{params}"

        context = {
            'obj': self.list_obj,
            'num_children': paginator.count,
            'more_url'    : more_url,
            'style': self.display_style,
        }
        try:
            context['page'] = paginator.page(no_page)
        except (EmptyPage, InvalidPage):
            context['page'] = paginator.page(paginator.num_pages)
        return context

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

class PaginatedReverseObjectsView(PaginatedObjectsView):
    named_url     = 'reverse_objects'
    display_style = 'middle'
    def get_queryset(self):
        self.list_obj = get_object_or_404(BaseObject, pk=int(self.kwargs['obj_id']))
        return self.list_obj.get_reverse_objects()

def invent_list(request):
    """ List all viewable inventories of first level on the site """
    if request.user.is_authenticated:
        if request.user.is_superuser:
            invs = BaseObject.objects.filter(obj_type__code='inventory', level=0)
        else:
            invs = BaseObject.objects.filter(obj_type__code='inventory', level=0).filter(Q(status='public') | (Q(owner=request.user)))
    else:
        invs = BaseObject.objects.filter(obj_type__code='inventory', level=0, status='public')
    invs = invs.order_by('title')
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    paginator = Paginator(invs, 12)
    context = {'invents': paginator.page(page), 'breadcrumb': [HOME_TUPLE, (_("Inventory list"), "")]}
    return render(request, 'invent/invent_list.html', context)


class ObjectEditView(LoginRequiredMixin, UpdateView):
    model = BaseObject
    template_name = 'invent/object_edit.html'
    form_class = forms.ObjectEditForm
    pk_url_kwarg = 'obj_id'

    def get_object(self):
        obj = super().get_object()
        self.obj_type = obj.obj_type
        return obj

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'user': self.request.user, 'obj_type': self.obj_type})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'obj': self.object,
            'breadcrumb': self.object and self.object.breadcrumb() or '',
        })
        return context

    def form_valid(self, form):
        if not form.instance.owner_id:
            form.instance.owner = self.request.user
        if 'parent_id' in self.kwargs and int(self.kwargs['parent_id']) != 0:
            # In case of creation
            parent = BaseObject.objects.get(pk=self.kwargs['parent_id'])
            form.instance.parent = parent
        form.save()

        if form.instance.obj_type.code == 'image':
            redirect_url = "%s#%d" % (form.instance.parent.get_absolute_url(), form.instance.id)
        else:
            redirect_url = form.instance.get_absolute_url()
        return HttpResponseRedirect(redirect_url)


class ObjectCreateView(ObjectEditView):
    def dispatch(self, *args, **kwargs):
        self.obj_type = get_object_or_404(ObjectType, code=self.kwargs['obj_code'])
        return super().dispatch(*args, **kwargs)

    def get_object(self):
        return None

    def get_initial(self):
        return {'obj_type': self.obj_type.pk}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'breadcrumb': [HOME_TUPLE, (_("Create object"), "")],
        })
        return context


@login_required
def object_edit_perms(request, obj_id):
    obj = get_object_or_404(BaseObject, pk=obj_id)
    if not obj.can_edit(request.user):
        return HttpResponseForbidden(_("You are not allowed to edit this object"))
    edit_form = PermissionEditForm(obj, request.POST or None)
    if request.method == 'POST':
        if edit_form.is_valid():
            edit_form.save()
            return HttpResponseRedirect(edit_form.instance.get_absolute_url())
    context = {
        'obj': obj,
        'breadcrumb': obj.breadcrumb(),
        'form'  : edit_form,
    }
    return render(request, 'invent/object_edit_perms.html', context)


@login_required
@require_POST
def object_delete(request, obj_id):
    """ Delete an object """
    obj = get_object_or_404(BaseObject, pk=obj_id)
    if not obj.can_edit(request.user):
        return HttpResponseForbidden(_("You are not allowed to delete this object"))
    parent = obj.parent
    title = obj.title
    obj.delete()
    messages.success(request, _("The object '%s' has been deleted.") % title)
    if parent:
        return object_detail(request, parent.id)
    else:
        return HttpResponseRedirect(reverse('home'))

@login_required
def object_import(request, obj_id):
    """ Import an xls file containing data for sub-objects creation """
    obj = get_object_or_404(BaseObject, pk=obj_id)
    if not obj.can_edit(request.user):
        return HttpResponseForbidden(_("You are not allowed to edit this object"))
    import_form = forms.ImportForm(request.POST or None, request.FILES or None)
    msg = ""
    if request.method == 'POST':
        if import_form.is_valid():
            try:
                msg = import_file(request.FILES['xlsfile'], import_form.cleaned_data['object_type'],
                                  request.user, obj, update=import_form.cleaned_data['is_update'])
            except ValueError as err:
                msg = err
            import_form = None
    context = {
        'parent': obj,
        'form'  : import_form,
        'message' : msg
    }
    return render(request, 'invent/object_import.html', context)

# Views for attribute editing
def add_value(request, object_id, attr_id):
    objet = get_object_or_404(BaseObject, pk=object_id)
    attr = get_object_or_404(Attribute, pk=attr_id)
    form = forms.get_value_form(objet, attr, None, request.POST or None)
    if request.method == 'POST':
        if not objet.can_edit(request.user):
            return HttpResponseForbidden(_("You are not allowed to edit this object"))
        if form.is_valid():
            newval = MetaValue(attr, objet)
            try:
                newval.set_value(form.cleaned_data)
            except ValidationError as e:
                return HttpResponseForbidden(_(u"Error: %s") % u", ".join(e.messages))
            return HttpResponse("<div>%s</div>" % newval.render(context=Context({})))
        else:
            return HttpResponseForbidden(form_errors_to_string(form.errors))
    # Get value widget form
    context = {'value_form': form.as_p(), 'value_id': 0, 'STATIC_URL': settings.STATIC_URL }
    return render(request, 'invent/attribute_edit.html', context)

def edit_value(request, object_id, value_id):
    objet = get_object_or_404(BaseObject, pk=object_id)
    try:
        value_id = int(value_id)
        values = [get_object_or_404(ObjectValue, pk=value_id).get_proxied()]
        attr = values[0].attr
    except ValueError:
        # value_id is an attribute name
        attr = get_object_or_404(Attribute, name=value_id)
        values =list(ObjectValue.objects.filter(obj=objet, attr=attr))
    if attr.multivalued:
        value_obj = MetaValue(attr, objet, values)
    else:
        value_obj = values[0]
    form = forms.get_value_form(objet, value_obj.attr, value_obj, request.POST or None, with_label=False)
    if request.method == 'POST':
        if not objet.can_edit(request.user):
            return HttpResponseForbidden(_("You are not allowed to edit this object"))
        if form.is_valid():
            value_obj.set_value(form.cleaned_data)
            objet.save()  # trigger re-index
            return HttpResponse("%s" % value_obj.render(context=Context({})))
        else:
            return HttpResponseForbidden(form_errors_to_string(form.errors))
    context = {'value_form': form.as_p(), 'value_id': value_obj.id, 'STATIC_URL': settings.STATIC_URL }
    return render(request, 'invent/attribute_edit.html', context)


@require_POST
def del_element(request, object_id, DelModel):
    """ Delete an attribute value from an object """
    objet = get_object_or_404(BaseObject, pk=object_id)
    if not objet.can_edit(request.user):
        return HttpResponseForbidden(_("You are not allowed to edit this object"))
    try:
        pk = int(request.POST['id_to_delete'])
        value = get_object_or_404(DelModel, pk=pk)
    except ValueError:
        attr = get_object_or_404(Attribute, name=request.POST['id_to_delete'])
        value = MetaValue(attr, objet, list(ObjectValue.objects.filter(obj=objet, attr=attr)))
    value.delete()
    objet.save()  # Re-index
    return HttpResponse("")

def del_value(request, object_id):
    return del_element(request, object_id, ObjectValue)

def add_photo(request, object_id):
    objet = get_object_or_404(BaseObject, pk=object_id)
    if request.method == 'POST':
        photo_form = forms.AddPhotoForm(request.POST, request.FILES)
        if photo_form.is_valid():
            photo_form.save(request, objet)
            return HttpResponseRedirect(objet.get_absolute_url())
    else:
        photo_form = forms.AddPhotoForm()
    context = {
        'object': objet,
        'form': photo_form,
    }
    return render(request, 'invent/photo_edit.html', context)

def edit_photo(request, object_id, photo_id):
    """ If photo_id = 0 -> new photo """
    objet = get_object_or_404(BaseObject, pk=object_id)
    photo_id = int(photo_id)
    photo = None
    if photo_id > 0:
        photo = PhotoAttachment.objects.get(pk=photo_id)
    if request.method == 'POST':
        photo_form = forms.AddPhotoForm(request.POST, request.FILES)
        if photo_form.is_valid():
            photo_form.save(request, objet)
            return HttpResponseRedirect(objet.get_absolute_url())
    else:
        photo_form = forms.AddPhotoForm()
    context = {
        'object': objet,
        'photo': photo,
        'form': photo_form,
    }
    return render(request, 'invent/photo_edit.html', context)

def del_photo(request, object_id):
    return del_element(request, object_id, PhotoAttachment)

def edit_file(request, object_id, file_id):
    objet = get_object_or_404(BaseObject, pk=object_id)
    file_id = int(file_id)
    file_obj = None
    if file_id > 0:
        file_obj = FileAttachment.objects.get(pk=file_id)
    if request.method == 'POST':
        file_form = forms.FileForm(request.POST, request.FILES, instance=file_obj)
        if file_form.is_valid():
            file_form.save(objet)
            return HttpResponseRedirect(objet.get_absolute_url())
    else:
        file_form = forms.FileForm(instance=file_obj)
    context = {
        'object': objet,
        'photo': file_obj,
        'form': file_form,
    }
    return render(request, 'invent/file_edit.html', context)

def del_file(request, object_id):
    return del_element(request, object_id, FileAttachment)

def attributes_in_group(request, group_id):
    """ Negative group_id indicates that this is other attributes for an objet type """
    group_id = int(group_id)
    if group_id < 0:
        obj_type = ObjectType.objects.get(pk = group_id * -1)
        attrs = AttributeForObject.get_others_grouped(obj_type).get_attributes()
    else:
        attrs = Attribute.objects.filter(attributeingroup__group__pk=group_id, active=True)
    attributes = [{'id':a.id, 'name':a.name, 'title':a.atitle, 'dyn':bool(a.calculator)} for a in attrs]
    return HttpResponse(json.dumps(attributes), content_type='application/json')

def attributes(request):
    """ Show all attributes grouped on a page """
    context = {
        'breadcrumb': [HOME_TUPLE, (_("Attribute List"), "")],
        'attr_groups': AttributeGroup.objects.all().order_by('position'),
        'attrs_no_group': Attribute.objects.filter(active=True, group__isnull=True).order_by('name'),
    }
    return render(request, 'invent/attributes.html', context)

def vocab_for_attribute(request):
    """ Returns all vocabulary values for an attribute vocabulary, in JSON format for Ajax use """
    attr_name, subattr_name, prefix = forms.SearchForm.decode_attr_name(request.GET.get('attr_name'))
    if subattr_name:
        attr_name = subattr_name
    if not re.fullmatch(r"[-a-zA-Z0-9_]+", attr_name):
        raise Http404("Bad attribute name")
    try:
        attr = Attribute.objects.select_related('vocab').get(name=attr_name)
    except Attribute.DoesNotExist:
        raise Http404
    voc_values = [{'id':v['id'],'name':v['name']} for v in attr.get_vocab()]
    return HttpResponse(json.dumps(voc_values), content_type='application/json')


class SearchView(TemplateView):
    """
    SearchView has three possible outputs:
    - "list" (default) renders the standard result
    - "export-options" renders the options for export
    - "export" exports in Excel file (xlsx)
    """

    template_name = "search/search.html"
    output = "list"
    form_class = forms.SearchForm

    def get(self, request, *args, **kwargs):
        search_form = self.form_class(request.GET or None, request.user)
        results = self.get_search_results(search_form)
        if self.output == "export-options":
            return self.render_export_options_page(
                forms.SearchExportOptionsForm(self.get_attributes_by_type(results)),
                search_form,
                *args,
                **kwargs,
            )
        elif self.output == "export" and results:
            return self.export_results(results.select_related("taxon"))
        return self.render_search_page(search_form, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """
        Post method is used to handle export fields only.
        """
        search_form = self.form_class(request.GET or None, request.user)
        results = self.get_search_results(search_form)
        if self.output != "export" or not search_form.is_valid() or not results:
            messages.error(request, _("Issue to export your attributes"))
            return redirect(self.url_search_with_get)
        options_form = forms.SearchExportOptionsForm(
            self.get_attributes_by_type(results), data=request.POST
        )
        if not options_form.is_valid():
            return self.render_export_options_page(
                options_form, search_form, *args, **kwargs
            )
        attributes = Attribute.objects.filter(
            id__in=options_form.cleaned_data["attributes"]
        )
        return self.export_results(results.select_related("taxon"), attributes)

    def export_results(self, results, attributes=None):
        if not attributes:
            fields = ["ACCENUMB", "COLLDATE", "HOSTDESCR", "OTHER_SUBSTRATE"]
        else:
            fields = [att.name for att in attributes]

        if 'ITS_SEQUENCE' in fields:
            results = results.prefetch_related('bioentry_set')
        get_label_name = forms.SearchExportOptionsForm.get_field_label_name
        get_col_width = forms.SearchExportOptionsForm.get_field_col_width
        headers = [get_label_name(field_name) for field_name in fields]
        col_widths = [get_col_width(field_name) for field_name in fields]

        # "fungal name" as first column is the same for all exports
        headers = ["Fungal name"] + headers
        col_widths = [32] + col_widths

        export = OpenXMLExport("mycoscope.ch-results")
        export.write_line(headers, bold=True, col_widths=col_widths)

        def get_value(res, fname):
            if fname == 'ITS_SEQUENCE':
                return "\r\n".join([be.format('fasta') for be in res.bioentry_set.all()])
            return res.get_value_for(fname)

        for res in results:
            line_values = [re.sub(r" \(\d+\)", "", res.title)]
            line_values += [get_value(res, field_name) for field_name in fields]
            export.write_line(line_values)
        return export.get_http_response("mycoscope.ch-results.xlsx")

    def render_export_options_page(
        self, export_options_form, search_form, *args, **kwargs
    ):
        return render(
            self.request,
            "search/search_exports_options.html",
            {
                "breadcrumb": [
                    HOME_TUPLE,
                    (_("Search"), self.url_search_with_get),
                    (_("Export"), ""),
                ],
                "results_nb": self.get_search_results(search_form).count(),
                "form": export_options_form,
                "form_action_url": self.url_search_export_with_get,
            },
        )

    def render_search_page(self, search_form, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context.update(
            {
                "breadcrumb": [HOME_TUPLE, (_("Search"), "")],
                "form": search_form,
                "total": -1,  # Mark for 'no search done'
            }
        )
        results = self.get_search_results(search_form)
        if results is not None:
            results = results.values("id")
            paginator = Paginator(results, 40)
            no_page = int(self.request.GET.get("p", "1"))
            try:
                page = paginator.page(no_page)
            except (EmptyPage, InvalidPage):
                page = paginator.page(paginator.num_pages)
            # Transform object ids in real objects (only for current page)
            page.object_list = BaseObject.objects.filter(
                pk__in=[o.get("obj_id", o.get("id")) for o in page.object_list]
            )
            context.update(
                {
                    "page": page,
                    "total": paginator.count,
                    "qs": self.request.META[
                        "QUERY_STRING"
                    ],  # TODO remove existing p (.replace('&p=',''),
                }
            )
        return self.render_to_response(context)

    def get_attributes_by_type(self, search_results):
        attributes_by_type = {}
        types = ObjectType.objects.filter(baseobject__in=search_results).distinct()
        for t in types:
            for type_name, attributes in t.possible_attributes.items():
                if type_name not in attributes_by_type:
                    attributes_by_type[type_name] = []
                attributes_by_type[type_name] += attributes
                attributes_by_type[type_name] = list(
                    set(attributes_by_type[type_name])
                )
        spec_type = ObjectType.objects.get(code='specimen')
        if spec_type in types:
            attributes_by_type['Other Attributes'].extend(Attribute.objects.filter(name='ITS_SEQUENCE'))
        return attributes_by_type

    def get_search_results(self, search_form):
        self._search_results = None
        if self.request.GET:
            if search_form.is_valid():
                self._search_results = search_form.search()
        return self._search_results

    @property
    def url_search_with_get(self):
        url = reverse("search")
        return f"{url}?{urlencode(self.request.GET)}" if self.request.GET else url

    @property
    def url_search_export_with_get(self):
        url = reverse("search-export")
        return f"{url}?{urlencode(self.request.GET)}" if self.request.GET else url


def add_criterion(request, base_num):
    """ AJAX request to add a new criterion line in the search form """
    search_form = forms.SearchForm(data={}, user=request.user, base_num=int(base_num))
    t = Template("""
        <div class="search_crit">
            <div class="minus_icon" id="minus_icon_%d"><a href='#' title='%s' onclick='return del_criterion($(this));'>
                <img src='%simg/minus.png' alt='Remove criterion'/></a>
            </div>
            {{ crit_group.0 }}  {{ crit_group.1 }}  {{ crit_group.2 }} {{ crit_group.3 }}
        </div>
    """ % (int(base_num), _("Remove criterion"), settings.STATIC_URL))
    return HttpResponse(t.render(Context({'crit_group': list(search_form.criterion())[0]})))

# Utility functions
def form_errors_to_string(error_list):
    err_string = ", ".join(["%s: %s" % (k, "/".join([str(e) for e in err])) for k, err in error_list.items()])
    return err_string

def http_403(request, reason):
    return HttpResponseForbidden(
        loader.render_to_string('403.html', context={'reason': reason},  request=request)
    )

def tabular_view_allowed(user):
    if settings.ALLOW_TABULAR_VIEW == 'all':
        return True
    elif settings.ALLOW_TABULAR_VIEW == 'auth':
        return user.is_authenticated
    # finally: only admin
    elif user.is_anonymous:
        return False
    else:
        return user.is_siteadmin()

def is_ajax(request):
    return request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'
