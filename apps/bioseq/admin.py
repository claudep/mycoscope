from django.contrib import admin

from bioseq import models

class BioEntryAdmin(admin.ModelAdmin):
    raw_id_fields = ('taxon',)

admin.site.register(models.BioDatabase)
admin.site.register(models.BioEntry, BioEntryAdmin)
admin.site.register(models.BioSequence)
admin.site.register(models.BioEntryQualifierValue)
admin.site.register(models.SeqFeature)
admin.site.register(models.SeqFeatureQualifierValue)
admin.site.register(models.Ontology)
admin.site.register(models.Term)
admin.site.register(models.Reference)
admin.site.register(models.BlastResult)
