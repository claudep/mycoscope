from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('invent', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('taxo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BioDatabase',
            fields=[
                ('biodatabase_id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=128)),
                ('authority', models.CharField(db_index=True, max_length=128, null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'biodatabase',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BioEntry',
            fields=[
                ('bioentry_id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=40, db_index=True)),
                ('accession', models.CharField(max_length=128)),
                ('identifier', models.CharField(max_length=40, null=True, blank=True)),
                ('division', models.CharField(max_length=6, null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('version', models.IntegerField()),
            ],
            options={
                'db_table': 'bioentry',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BioentryDbxref',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rank', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'bioentry_dbxref',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BioEntryQualifierValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.TextField(null=True, blank=True)),
                ('rank', models.IntegerField(default=0)),
            ],
            options={
                'db_table': 'bioentry_qualifier_value',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BioentryReference',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_pos', models.IntegerField(null=True, blank=True)),
                ('end_pos', models.IntegerField(null=True, blank=True)),
                ('rank', models.IntegerField(default=0)),
            ],
            options={
                'db_table': 'bioentry_reference',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BioSequence',
            fields=[
                ('bioentry', models.OneToOneField(primary_key=True, serialize=False, to='bioseq.BioEntry', on_delete=models.CASCADE)),
                ('version', models.IntegerField(null=True, blank=True)),
                ('length', models.IntegerField(null=True, blank=True)),
                ('alphabet', models.CharField(max_length=10, null=True, blank=True)),
                ('seq', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'biosequence',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BlastResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('blast_cmd', models.CharField(max_length=250)),
                ('blast_date', models.DateTimeField(auto_now_add=True)),
                ('result_file', models.FileField(upload_to='blasts')),
                ('bioentry', models.ForeignKey(to='bioseq.BioEntry', on_delete=models.CASCADE)),
                ('blast_who', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.PROTECT)),
            ],
            options={
                'ordering': ['blast_date'],
                'db_table': 'blastfile',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('comment_id', models.AutoField(serialize=False, primary_key=True)),
                ('comment_text', models.TextField()),
                ('rank', models.IntegerField(default=0)),
                ('bioentry', models.ForeignKey(to='bioseq.BioEntry', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'comment',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Dbxref',
            fields=[
                ('dbxref_id', models.AutoField(serialize=False, primary_key=True)),
                ('dbname', models.CharField(max_length=40, db_index=True)),
                ('accession', models.CharField(max_length=128)),
                ('version', models.IntegerField()),
            ],
            options={
                'db_table': 'dbxref',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('location_id', models.AutoField(serialize=False, primary_key=True)),
                ('start_pos', models.IntegerField(null=True, blank=True)),
                ('end_pos', models.IntegerField(null=True, blank=True)),
                ('strand', models.IntegerField(default=0)),
                ('rank', models.IntegerField(default=0)),
                ('dbxref', models.ForeignKey(blank=True, to='bioseq.Dbxref', null=True, on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'location',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LocationQualifierValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=255)),
                ('int_value', models.IntegerField(null=True, blank=True)),
                ('location', models.ForeignKey(to='bioseq.Location', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'location_qualifier_value',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Ontology',
            fields=[
                ('ontology_id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=32)),
                ('definition', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'ontology',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Reference',
            fields=[
                ('reference_id', models.AutoField(serialize=False, primary_key=True)),
                ('location', models.TextField()),
                ('title', models.TextField(null=True, blank=True)),
                ('authors', models.TextField(null=True, blank=True)),
                ('crc', models.CharField(max_length=32, unique=True, null=True, blank=True)),
                ('dbxref', models.OneToOneField(null=True, blank=True, to='bioseq.Dbxref', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'reference',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SeqFeature',
            fields=[
                ('seqfeature_id', models.AutoField(serialize=False, primary_key=True)),
                ('display_name', models.CharField(max_length=64, null=True, blank=True)),
                ('rank', models.IntegerField(default=0)),
                ('bioentry', models.ForeignKey(to='bioseq.BioEntry', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'seqfeature',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SeqFeatureDbxref',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rank', models.IntegerField(null=True, blank=True)),
                ('dbxref', models.ForeignKey(to='bioseq.Dbxref', on_delete=models.CASCADE)),
                ('seqfeature', models.ForeignKey(to='bioseq.SeqFeature', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'seqfeature_dbxref',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SeqFeatureQualifierValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rank', models.IntegerField(default=0)),
                ('value', models.TextField()),
                ('seqfeature', models.ForeignKey(to='bioseq.SeqFeature', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'seqfeature_qualifier_value',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Term',
            fields=[
                ('term_id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('definition', models.TextField(null=True, blank=True)),
                ('identifier', models.CharField(max_length=40, unique=True, null=True, blank=True)),
                ('is_obsolete', models.CharField(max_length=1, null=True, blank=True)),
                ('ontology', models.ForeignKey(to='bioseq.Ontology', on_delete=models.PROTECT)),
            ],
            options={
                'db_table': 'term',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='term',
            unique_together=set([('name', 'ontology', 'is_obsolete')]),
        ),
        migrations.AddField(
            model_name='seqfeaturequalifiervalue',
            name='term',
            field=models.ForeignKey(to='bioseq.Term', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='seqfeaturequalifiervalue',
            unique_together=set([('seqfeature', 'term', 'rank')]),
        ),
        migrations.AlterUniqueTogether(
            name='seqfeaturedbxref',
            unique_together=set([('seqfeature', 'dbxref')]),
        ),
        migrations.AddField(
            model_name='seqfeature',
            name='dbxrefs',
            field=models.ManyToManyField(to='bioseq.Dbxref', through='bioseq.SeqFeatureDbxref'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='seqfeature',
            name='source_term',
            field=models.ForeignKey(related_name='source_term', to='bioseq.Term', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='seqfeature',
            name='type_term',
            field=models.ForeignKey(related_name='type_term', to='bioseq.Term', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='seqfeature',
            unique_together=set([('bioentry', 'type_term', 'source_term', 'rank')]),
        ),
        migrations.AddField(
            model_name='locationqualifiervalue',
            name='term',
            field=models.ForeignKey(to='bioseq.Term', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='locationqualifiervalue',
            unique_together=set([('location', 'term')]),
        ),
        migrations.AddField(
            model_name='location',
            name='seqfeature',
            field=models.ForeignKey(to='bioseq.SeqFeature', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='location',
            name='term',
            field=models.ForeignKey(blank=True, to='bioseq.Term', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='location',
            unique_together=set([('seqfeature', 'rank')]),
        ),
        migrations.AlterUniqueTogether(
            name='dbxref',
            unique_together=set([('accession', 'dbname', 'version')]),
        ),
        migrations.AlterUniqueTogether(
            name='comment',
            unique_together=set([('bioentry', 'rank')]),
        ),
        migrations.AddField(
            model_name='bioentryreference',
            name='bioentry',
            field=models.ForeignKey(to='bioseq.BioEntry', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bioentryreference',
            name='reference',
            field=models.ForeignKey(to='bioseq.Reference', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='bioentryreference',
            unique_together=set([('bioentry', 'reference', 'rank')]),
        ),
        migrations.AddField(
            model_name='bioentryqualifiervalue',
            name='bioentry',
            field=models.ForeignKey(to='bioseq.BioEntry', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bioentryqualifiervalue',
            name='term',
            field=models.ForeignKey(to='bioseq.Term', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='bioentryqualifiervalue',
            unique_together=set([('bioentry', 'term', 'rank')]),
        ),
        migrations.AddField(
            model_name='bioentrydbxref',
            name='bioentry',
            field=models.ForeignKey(to='bioseq.BioEntry', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bioentrydbxref',
            name='dbxref',
            field=models.ForeignKey(to='bioseq.Dbxref', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='bioentrydbxref',
            unique_together=set([('bioentry', 'dbxref')]),
        ),
        migrations.AddField(
            model_name='bioentry',
            name='baseobjects',
            field=models.ManyToManyField(to='invent.BaseObject', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bioentry',
            name='biodatabase',
            field=models.ForeignKey(to='bioseq.BioDatabase', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bioentry',
            name='dbxrefs',
            field=models.ManyToManyField(to='bioseq.Dbxref', through='bioseq.BioentryDbxref'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bioentry',
            name='references',
            field=models.ManyToManyField(to='bioseq.Reference', through='bioseq.BioentryReference'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bioentry',
            name='taxon',
            field=models.ForeignKey(to='taxo.Taxon', null=True, on_delete=models.SET_NULL),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='bioentry',
            unique_together=set([('identifier', 'biodatabase'), ('accession', 'biodatabase', 'version')]),
        ),
    ]
