import urllib
from xml.etree.ElementTree import parse
from optparse import make_option

from django.core.management.base import BaseCommand

from taxo.models import Taxon, TaxonSource, TaxonSourceTaxon

# See:
# http://data.gbif.org/ws/rest/taxon
# http://wiki.gbif.org/dadiwiki/wikka.php?wakka=DeveloperAPIs

class Command(BaseCommand):
    """ Populate taxonomic tree from GBIF data from Catalog of Life provider (id=2) """

    PROVIDERS = {
        # Species 2000 & ITIS Catalogue of Life Hierarchy, Edition 1 (2007))
        'COL2007': ('Catalogue of Life (2007)', 2),
        'IF2007' : ('Index Fungorum (2007)', 223),
    }
    kingdom_url = "http://data.gbif.org/ws/rest/taxon/list?rank=kingdom&dataproviderkey=%d"
    taxon_url = "http://data.gbif.org/ws/rest/taxon/get?key=%d&dataproviderkey=%d"
    tc_namespace = "{http://rs.tdwg.org/ontology/voc/TaxonConcept#}"
    tn_namespace = "{http://rs.tdwg.org/ontology/voc/TaxonName#}"
    gbif_namespace = "{http://portal.gbif.org/ws/response/gbif}"
    rdf_namespace = "{http://www.w3.org/1999/02/22-rdf-syntax-ns#}"
    rank_mapping = {
        'kingdom': 'regnum',
        'phylum': 'divisio',
        'class': 'classis',
        'order': 'ordo',
        'family': 'familia',
        'genus': 'genus',
        'species': 'species',
        'subspecies': 'subspecies',
        'variety': 'varietas'
    }
    status_mapping = {
        'accepted': 'A',
    }

    option_list = BaseCommand.option_list + (
        make_option('--provider', dest='provider',
            help="specify the provider source on the GBIF (default to COL2007, Cat. of Life)"),
        make_option('--start-at', dest='tx_start',
            help="specify the source id to start populate from"),
    )
    
    def handle(self, *args, **options):        
        # begin from higher rank (kingdom)
        self.provider = options.get('provider') or 'COL2007'
        if self.provider not in self.PROVIDERS:
            return "Sorry, the provider code '%s' is not known." % self.provider

        self.source, cr = TaxonSource.objects.get_or_create(name=self.provider)
        provider_key = self.PROVIDERS[self.provider][1]
        if options.get('tx_start'):
            try:
                tx =  Taxon.objects.get_by_sourceid(self.source, options.get('tx_start'))
            except Taxon.DoesNotExist:
                return "Sorry the taxon id '%s' does not exist in the database for the source %s" % (options.get('tx_start'), self.source.name)
            self.populate(tx)
        else:
            start_url = self.kingdom_url % provider_key
            txdoc = parse(urllib.urlopen(start_url)).getroot()
            for taxon in txdoc.findall(".//%sTaxonConcept" % self.tc_namespace):
                key = taxon.attrib['gbifKey']
                txname = taxon.find("%shasName" % self.tc_namespace).find(
                        "%sTaxonName" % self.tn_namespace).findtext(
                        "%snameComplete" % self.tn_namespace)
                tx, cr = Taxon.objects.get_or_create(name=txname, rank='regnum', status='A')
                if cr:
                    print "Kingdom '%s' created" % txname
                ts, cr = TaxonSourceTaxon.objects.get_or_create(source=self.source, taxon=tx, id_in_source=int(key))

                res = raw_input(u"Do you want to retrieve '%s' hierarchy? (y): " % txname)
                if res == 'y':
                    self.populate(tx)
    
    def populate(self, parent, only_if_created=True):
        source_id = parent.get_id_in_source(self.source)
        url_file = urllib.urlopen(self.taxon_url % (source_id, self.PROVIDERS[self.provider][1]))
        try:
            txdoc = parse(url_file).getroot()
        except Exception, e:
            import pdb; pdb.set_trace()
            res = raw_input(u"Parse error in retrieved file. Quit (q) or continue (c)?")
            if res == "c":
                return
            else:
                raise e

        # get taxon concept (parent)
        tx_concepts = txdoc.find(".//%staxonConcepts" % self.gbif_namespace)
        main_tx = tx_concepts[0]
        rels = main_tx.findall("*/%sRelationship" % self.tc_namespace)
        child_taxon_keys = []
        for rel in rels:
            relcat = rel.find("%srelationshipCategory" % self.tc_namespace)
            if relcat.attrib['%sresource' % self.rdf_namespace].endswith("IsParentTaxonOf"):
                child_url = rel.find("%stoTaxon" % self.tc_namespace).attrib['%sresource' % self.rdf_namespace]
                # get key of toTaxon, <tc:toTaxon rdf:resource="http://data.gbif.org/ws/rest/taxon/get/11314284"/>
                child_key = child_url.split("/")[-1]
                child_taxon_keys.append(child_key)
        taxons = tx_concepts.findall("%sTaxonConcept" % self.tc_namespace)
        for taxon in taxons:
            if taxon.attrib['gbifKey'] in child_taxon_keys:
                # extract child data and insert into DB
                status = taxon.attrib['status']
                status = self.status_mapping.get(status, status)
                txname = taxon.find("%shasName" % self.tc_namespace).find(
                    "%sTaxonName" % self.tn_namespace).findtext(
                    "%snameComplete" % self.tn_namespace)
                rank = taxon.find("%shasName" % self.tc_namespace).find(
                    "%sTaxonName" % self.tn_namespace).findtext(
                    "%srankString" % self.tn_namespace)
                rank = self.rank_mapping.get(rank, rank)
                gbif_id=int(taxon.attrib['gbifKey'])
                
                # Test if already existing (same gbif id)
                created = False; tst_cr = False
                try:
                    tx = Taxon.objects.get_by_sourceid(self.source, gbif_id)
                except Taxon.DoesNotExist:
                    # Test if existing in another taxonomy + Add source
                    tx, created = Taxon.objects.get_or_create(name=txname, parent=parent, rank=rank, defaults={'status': status})
                    
                    # It may happen that a same name is twice in a same source (different author), but as authors cannot currently
                    # be retrieved, we do not create the second TST in the database
                    tst, tst_cr = TaxonSourceTaxon.objects.get_or_create(source=self.source, taxon=tx, defaults={'id_in_source' : gbif_id})
                if created:
                    print "Added '%s' (%s) in taxonomy" % (tx.full_name, tx.rank)
                if not only_if_created or (only_if_created and (created or tst_cr)):
                    self.populate(tx)
