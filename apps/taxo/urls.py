from django.urls import path
from . import views

urlpatterns = [
    path('search/<mode>/', views.taxo_search, name='taxo_search'),
    path('browse/<int:taxon_id>/<mode>/', views.taxo_browse_by_mode, name='taxo_browse_by_mode'),
    path('browse/<int:taxon_id>/', views.taxo_browse, name='taxo_browse'),
    # 2 URLs to retrieve only object list (AJAX):
    #path('<int:taxon_id>/subtaxons/', views.taxo_browse_subtaxons, name='taxo_browse_subtaxons'),
    path('<int:taxon_id>/subobjects/', views.taxo_browse_subobjects, name='taxo_browse_subobjects'),

    path('by_rank/<rank>/<mode>/', views.taxo_tree_by_rank, name='taxo_tree_by_rank'),
    path('<int:taxon_id>/select/<mode>/', views.taxo_tree, name='taxo_tree'),
]
