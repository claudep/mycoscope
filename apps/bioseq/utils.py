# Copyright (c) 2010 BCIS/Claude Paroz <claude@2xlibre.net>.

import codecs
import os
import tempfile
import logging
from io import StringIO
from subprocess import Popen, PIPE

from Bio import SeqIO, Entrez
from bioseq.models import BioEntry, BioDatabase, DBSeqRecord
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import transaction
from django.utils.translation import gettext as _
from django.utils.encoding import force_str

BLASTDB_TITLE = "%s_blast" % settings.DATABASES['default']['NAME']

class UnsupportedFileFormat(Exception):
    pass

def import_sequence(obj, seqfile=None, acc_number=None, target='current'):
    assert seqfile or acc_number
    if seqfile:
        seq_records = import_sequence_from_file(seqfile)
    else:
        try:
            # if sequence already imported for another accession, just add a reference
            be = BioEntry.objects.get(accession = acc_number)
            be.baseobjects.add(obj)
            return be
        except BioEntry.DoesNotExist:
            seq_records = [import_sequence_from_ncbi(acc_number)]
    db = BioDatabase.objects.get(name='main')
    bioentries = []
    errs = []
    for rec in seq_records:
        if target == 'current':
            rec.base_object = obj
        else: # 'any'
            candidates = obj.__class__.get_by_attr_value('ACCENUMB', rec.id)
            candidates = [c for c in candidates if c.parent == obj.parent]
            if len(candidates) < 1:
                errs.append("There are no accessions with accession number '%s'" % rec.id)
            elif len(candidates) > 1:
                errs.append("There are several accessions with accession number '%s'" % rec.id)
            else:
                rec.base_object = candidates[0]
    if errs:
        raise ValidationError([ValidationError(err) for err in errs])
    else:
        with transaction.atomic():
            for rec in seq_records:
                for existing in rec.base_object.bioentry_set.all():
                    if existing.biosequence.seq == str(rec.seq):
                        # Already existing sequence, not importing
                        break
                else:
                    # Save sequence in database
                    bioentries.extend(db.load([rec]))
    return bioentries


def import_sequence_from_file(sequence_file):
    """ Import a sequence file and return the imported sequences """
    records = []
    format_ = guess_format(sequence_file)
    encoding, bom_len = sniff_encoding(sequence_file)
    sequence_path = sequence_file.temporary_file_path()
    if encoding is not None:
        # Special case for text files with BOM marks
        import io
        sequence_file = io.open(sequence_path, 'r', encoding=encoding)
        sequence_file.seek(bom_len)

    # Check encoding, if not UTF-8 encoded, read as latin-1
    file_content = sequence_file.read()
    try:
        sequence_file = StringIO(file_content.decode('utf-8'))
    except UnicodeDecodeError:
        sequence_file = StringIO(file_content.decode('latin-1'))

    if format_ == 'txt_custom':
        for seq_record in read_seqs_from_txt_custom(sequence_file):
            records.append(seq_record)
    else:
        for seq_record in SeqIO.parse(sequence_file, format_):
            if str(seq_record.seq) != '':
                records.append(seq_record)
    return records


def import_sequence_from_ncbi(acc_name):
    Entrez.email = "info@bcis.ch"
    Entrez.tool = "BCIS Web application"
    handle = Entrez.esearch(db="nucleotide", term="%s[ACCN]" % acc_name)
    record = Entrez.read(handle)
    handle.close()
    if int(record["Count"]) == 0:
        raise Exception(_("No results with accession number '%s' in Entrez nucleotide database.") % acc_name)
    #FIXME: continue implementation...
    handle = Entrez.efetch(db="nucleotide", id=record["IdList"][0], rettype="gb")
    seq_record = SeqIO.read(handle, "genbank")
    return seq_record


def sniff_encoding(open_file):
    first_line = open_file.readline()
    open_file.seek(0)
    BOMS = (
        (codecs.BOM_UTF8, "UTF-8"),
        (codecs.BOM_UTF32_BE, "UTF-32-BE"),
        (codecs.BOM_UTF32_LE, "UTF-32-LE"),
        (codecs.BOM_UTF16_BE, "UTF-16-BE"),
        (codecs.BOM_UTF16_LE, "UTF-16-LE"),
    )
    encoding, bom_len = next(iter([
        (encoding, len(bom)) for bom, encoding in BOMS if first_line.startswith(bom)
    ]), (None, 0))
    return encoding, bom_len


def read_seqs_from_txt_custom(sequence_file):
    descr = seq = ''

    def to_fasta(descr, seq):
        accenum, descr2 = descr.split(maxsplit=1)
        accenum = accenum.replace('M_', '')
        fasta = StringIO('>%s %s\n%s' % (accenum, descr2, seq))
        return SeqIO.read(fasta, "fasta")

    for line in sequence_file:
        if not descr:
            descr = force_str(line.strip())
            continue
        if not seq:
            seq = force_str(line.strip())
            continue
        yield to_fasta(descr, seq)
        descr = seq = ''
    if descr:
        yield to_fasta(descr, seq)



def guess_format(sequence_file):
    extension = sequence_file.name.rsplit(".", 1)[1]
    # Try to guess by extension
    if extension in ("fas", "fasta"):
        return "fasta"
    elif extension in ("gb", "gbk", "genbank"):
        return "genbank"
    # Try to guess by first line content
    _, bom_len = sniff_encoding(sequence_file)
    first_line = force_str(sequence_file.readline(), errors='replace')
    sequence_file.seek(0)
    if bom_len:
        first_line = first_line[bom_len:]
    if first_line.startswith(">"):
        return "fasta"
    elif first_line.startswith("LOCUS"):
        return "genbank"
    if extension == "txt":
        # As last resort, it might be a custom mycoscope txt format.
        return 'txt_custom'
    raise UnsupportedFileFormat()


# ********* BLAST related utilities ****************

def make_blast_db():
    """ Create a local blast database with all existing sequences """
    # Export all sequences in a FASTA file
    out_file = tempfile.NamedTemporaryFile(mode='w')
    SeqIO.write([DBSeqRecord(seq) for seq in BioEntry.objects.all()], out_file, 'fasta')
    out_file.flush()
    # Create the database with makeblastdb cl tool
    cmd = "makeblastdb -in %(input)s -dbtype='nucl' -parse_seqids -hash_index -title='%(title)s' -out %(output)s" % {
        'input': out_file.name, 'output': settings.BLASTDB_PATH, 'title': BLASTDB_TITLE,
        }
    logging.info(cmd)
    pipe = Popen(cmd, shell=True, env=None, stdin=None, stdout=PIPE, stderr=PIPE)
    (output, errout) = pipe.communicate()
    status = pipe.returncode
    if status == 0:
        logging.info("makeblastdb ran successfully")
    else:
        logging.error("Command error status: %s\n%s" % (status,errout))
    out_file.close()

def blastn(raw_seq):
    env = {'BLASTDB': os.path.dirname(settings.BLASTDB_PATH)}
    if not os.path.exists('%s.nsd' % settings.BLASTDB_PATH):
        make_blast_db()
    cmd = "blastn -db %(name)s -outfmt=5" % {'name': os.path.basename(settings.BLASTDB_PATH)}
    pipe = Popen(cmd, shell=True, env=env, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    pipe.stdin.write(raw_seq.encode('ascii'))
    (output, errout) = pipe.communicate()
    status = pipe.returncode
    if status == 0:
        return (cmd, output)
    else:
        raise Exception("Error running blastn: %s" % errout)

