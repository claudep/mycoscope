# Copyright (c) 2010,2016 Claude Paroz <claude@2xlibre.net>

from django import forms
from django.utils.translation import gettext_lazy as _

from bioseq.utils import guess_format, UnsupportedFileFormat
from .models import BioEntry


class ImportForm(forms.Form):
    seqfile = forms.FileField(label = _("File containing sequence(s)"), required=False)
    ncbi_accession = forms.CharField(label = _("NCBI accession number"), required=False)
    target = forms.ChoiceField(
        choices=(
            ('current', _("Attach all sequences to the current accession")),
            ('any', _("Map the sequence identifier with any accession number in the collection"))
        ), initial='current', widget=forms.RadioSelect)

    def clean(self):
        if not self.cleaned_data.get('seqfile') and not self.cleaned_data.get('ncbi_accession'):
            raise forms.ValidationError(_("Please provide either a file or a NCBI accession number"))
        return self.cleaned_data

    def clean_seqfile(self):
        seqf = self.cleaned_data['seqfile']
        if seqf:
            try:
                self.format = guess_format(seqf)
            except UnsupportedFileFormat:
                raise forms.ValidationError(_("Sorry, the file you provided has not been recognized as a sequence file format"))
        return seqf


class BioEntryForm(forms.ModelForm):
    class Meta:
        model = BioEntry
        fields = [
            "name", "accession", "identifier", "unite_number", "division", "description", "version"
        ]
