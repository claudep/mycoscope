# Copyright (c) 2009-2011 Claude Paroz <claude@2xlibre.net>
import os
import re
from collections import OrderedDict

from django import forms
from django.contrib import messages
from django.contrib.auth.models import AnonymousUser
from django.contrib.postgres.search import SearchQuery
from django.core.files.base import File
from django.db.models import Q
from django.forms.widgets import HiddenInput
from django.http import Http404
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import ngettext, gettext_lazy as _
from django.utils.safestring import mark_safe

from tinymce.widgets import TinyMCE

from .models import (
    BaseObject, ObjectType, ObjectValue, Attribute, ImageSubobject,
    AttributeForObject, FileAttachment, DatatypeChoices
)


def int_or_404(value):
    try:
        return int(value)
    except ValueError:
        raise Http404('An integer was expected')


class TaxonSelectWidget(forms.Widget):
    """ Hidden input and select button """
    input_type = 'hidden'
    def render(self, name, value, attrs=None):
        output = []
        local_attrs = self.build_attrs(attrs, type=self.input_type, name=name)
        url = reverse('taxo_tree_by_rank', args=['genus'])
        output.append(HiddenInput().render(name, value, local_attrs))
        output.append("<input type='button' id='taxon_selector' value='%s' onclick='return open_taxo(\"%s\")'/>" % (_("Select..."), url))
        return mark_safe('\n'.join(output))


class ObjectEditForm(forms.ModelForm):
    class Meta:
        model = BaseObject
        fields = ('parent', 'title', 'description', 'obj_type', 'status', 'taxon')
        widgets = {'description': TinyMCE}

    def __init__(self, user=None, obj_type=None, **kwargs):
        class FieldGroup(OrderedDict):
            """ FieldGroup allows to get a field in a list with form.__getitem__ to obtain a BoundField in template """
            def __iter__(self2):
                for attr_name in self2.keys():
                    yield self[attr_name]

        def create_fields_for_attrs(attr_list, grp):
            for attr in attr_list:
                initial_val = existing_values.pop(attr.name, None)
                ff = attr.get_form_field(initial=initial_val and initial_val[0] or None)
                if ff is None: continue
                ff.comments = initial_val and initial_val[1] or None
                self.fields[attr.name] = ff
                self.fields[attr.name].is_attr = True
                self.gr_fields[grp.gname][attr.name] = ff

        super().__init__(**kwargs)
        # Allow parent editing if parent is already an inventory
        if self.instance.parent and self.instance.parent.obj_type.code == 'inventory':
            self.fields['parent'].queryset = BaseObject.objects.filter_writable_for_user(user
                ).filter(obj_type__code='inventory').order_by('title')
        else:
            del self.fields['parent']
        if obj_type.code == 'specimen':
            self.fields['title'].label = 'Species'
            del self.fields["description"]
        existing_values = dict([val.attr.name, (val.get_value(raw=True), val.comments)] for val in self.instance.get_values())
        self.fields['title'].widget.attrs["style"] = "width:98%;"
        if obj_type.has_taxonomy():
            self.fields['taxon'].widget = forms.HiddenInput()
            taxon_rem_attr = Attribute.objects.get(name='REMARKS_TAXO')
            initial_val = existing_values.pop('REMARKS_TAXO', None)
            self.fields['REMARKS_TAXO'] = taxon_rem_attr.get_form_field(initial=initial_val and initial_val[0] or None)
            self.fields['REMARKS_TAXO'].is_attr = True
        else:
            del self.fields['taxon']
        self.fields['obj_type'].widget = forms.HiddenInput()
        if self.instance.pk:
            # For existing objects, status is edited in PermissionEditForm
            del self.fields['status']

        self.gr_fields = OrderedDict()
        for group in obj_type.get_groups(categ_exclude=['taxo'], with_not_grouped=True):
            self.gr_fields[group.gname] = FieldGroup()
            create_fields_for_attrs(group.get_attributes(), group)
            last_group = group
        remaining_attrs = Attribute.objects.filter(name__in=[existing_values.keys()])
        create_fields_for_attrs(remaining_attrs, last_group)
        if not self.gr_fields[last_group.gname]:
            # Delete last empty group (Other attributes)
            del self.gr_fields[last_group.gname]

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        # Save attribute values
        for key in self.changed_data:
            if hasattr(self.fields[key], "is_attr"):
                new_value = self.cleaned_data.get(key)
                if new_value:
                    self.instance.set_value_for(key, new_value)
                else:
                    # Existing value to delete
                    value_obj = self.instance.get_value_for(key, as_object=True)
                    if value_obj:
                        value_obj.delete()
        # One more save to trigger re-indexing
        self.instance.save()


# http://www.b-list.org/weblog/2008/nov/09/dynamic-forms/
def get_value_form(obj, attribute, value_obj=None, data=None, with_label=True):
    """ Obtain a form class dynamically depending on the attribute datatype """
    fields = {}
    if attribute.datatype == 'meta':
        obj_data = value_obj and value_obj.get_subvalues(raw=True) or {}
        for subd in attribute.subdescriptors.all():
            fields['value_%s' % subd.name] = subd.get_form_field(initial=obj_data.get(subd.name, None))
    else:
        obj_data = value_obj and value_obj.get_value(raw=True) or None
        fields['value_%s' % attribute.name] = attribute.get_form_field(
            initial=obj_data, with_label=with_label, required=True)
    # FIXME: comments in meta attribute needs some attention...
    if attribute.get_datatype() not in ('text', 'meta'):
        obj_data = value_obj and value_obj.comments or None
        fields['comments'] = forms.CharField(initial=obj_data, required=False, label=_("Comments"))
    for f in fields:
        fields[f].widget.attrs.update({'class':'form_value'})
    fields['descriptor'] = forms.IntegerField(widget=forms.HiddenInput, initial=attribute.id, required=False)
    fields['object']     = forms.IntegerField(widget=forms.HiddenInput, initial=obj.id, required=False)
    new_class = type('ValueForm', (forms.BaseForm,), { 'base_fields': fields })
    return new_class(data)


class AddPhotoForm(forms.Form):
    image  = forms.FileField(label=_("Image"))
    title  = forms.CharField(label=_("Title"), required=False,
        max_length=BaseObject._meta.get_field('title').max_length)

    authorized_exts = ('.gif', '.png', '.jpg', '.jpeg', '.zip')
    class Media:
        css = {
            'all': ('css/forms.css',)
        }

    def clean_image(self):
        data = self.cleaned_data['image']
        if data:
            ext = os.path.splitext(data.name)[1].lower()
            if ext not in self.authorized_exts:
                raise forms.ValidationError(_("Only files with following extensions are admitted: %s") % ", ".join(self.authorized_exts))
        return data

    def save(self, request, parent, *args, **kwargs):
        up_file = self.cleaned_data['image']
        if up_file.name.endswith(".zip"):
            import zipfile, tempfile
            files = []
            zfobj = zipfile.ZipFile(up_file, 'r')
            for name in zfobj.namelist():
                ext = ".%s" % name.rsplit('.', 1)[-1].lower()
                if ext in self.authorized_exts and not name.startswith("/") and not ".." in name:
                    tmp = tempfile.NamedTemporaryFile(suffix=ext)
                    tmp.write(zfobj.read(name))
                    files.append((File(tmp, name=name), name.rsplit('.', 1)[0]))
        else:
            files = [(up_file, self.cleaned_data.get('title', up_file.name.rsplit('.', 1)[0]))]
        for f, title in files:
            # Create base_object of type 'image'
            bo = BaseObject.objects.create(
                obj_type=ObjectType.objects.get(code='image'),
                parent=parent, status=parent.status, owner=parent.owner, title=title)
            # Create image_object
            io = ImageSubobject.objects.create(obj=bo, image=f)
        messages.success(request, ngettext("%d new image has been added.", "%d new images have been added.", len(files)) % len(files))


class FileForm(forms.ModelForm):
    class Meta:
        model = FileAttachment
        fields = ('fil', 'title')

    def save(self, obj, *args, **kwargs):
        if not self.cleaned_data['title']:
            # If no title entered, use file name as fallback
            self.cleaned_data['title'] = self.files['fil'].name
        self.instance.content_object = obj
        if 'fil' in self.files:
            self.instance.mimetype = self.files['fil'].content_type
        super().save(*args, **kwargs)


class ImportForm(forms.Form):
    xlsfile     = forms.FileField(label = _("File to import (.ods/.xlsx)"))
    object_type = forms.ChoiceField(label = _("Object type"), choices = ())
    is_update   = forms.BooleanField(label = _("This is an update"), required=False)

    def __init__(self, data=None, *args, **kwargs):
        super().__init__(data, *args, **kwargs)
        # Set in __init__, otherwise database is hit in *every* request
        self.fields['object_type'].choices = ObjectType.as_choices()
        # If found, specimen object type is default
        try:
            ot = ObjectType.objects.get(code="specimen")
            self.fields['object_type'].initial = ot.id
        except ObjectType.DoesNotExist:
            pass

    def clean_xlsfile(self):
        data = self.cleaned_data['xlsfile']
        supported = any(snip in data.content_type for snip in ('excel', 'spreadsheetml', 'opendocument.spreadsheet'))
        if not supported:
            raise forms.ValidationError(
                _("Sorry, this does not seem to be a valid ods or xls file.") + " (content_type=%s)" % data.content_type)
        return self.cleaned_data


class SimpleSearchForm(forms.Form):
    text = forms.CharField(
        label=_("Search"),
        widget=forms.TextInput(attrs={'class': 'autocomplete'})
    )

    def __init__(self, required=True, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["text"].required = required


class SearchForm(forms.Form):
    text = forms.CharField(label=_("Search"), required=False)
    obj_type_code = "specimen"

    class Media:
        js = ('js/search.js',)

    oper_type_map = {
        'T': (('icontains', _('contains')), ('exact', _('is exactly')),
              ('istartswith', _('begins with')), ('iendswith', _('ends with'))),  # Text
        'N': (('exact', '='), ('gt', '>'), ('gte', '>='), ('lte', '<='), ('lt', '<')),  #Number
        'D': (('lt', _('before')), ('exact', _('the')), ('gt', _('after'))),  # Date
        'B': (('exact', _('is')),),  # Boolean
        'V': (('exact', _('is')),),  # Vocabulary
    }

    @classmethod
    def decode_attr_name(cls, value):
        """ attribute name may include metadata : ATTR__SUBATTR or ..ATTR """
        attr_name = value
        subattr_name = prefix = u""
        if attr_name and '__' in attr_name:
            attr_name, subattr_name = value.split('__', 1)
        if attr_name and attr_name.startswith(".."):
            prefix = "parent__"
            attr_name = attr_name[2:]
        return attr_name, subattr_name, prefix

    def __init__(self, data=None, user=None, base_num=None, *args, **kwargs):
        super().__init__(data, *args, **kwargs)
        if user is None:
            user = AnonymousUser()
        self.user = user
        self.obj_type = ObjectType.objects.get(code=self.obj_type_code)
        attr_list = self._collect_attributes()
        attr_list.append(("T_TAXONOMY", _("Taxonomy")))
        attr_list.sort(key=lambda x: x[1])
        attr_list.insert(0,('', _('Choose an attribute')))

        self.field_nums = sorted(
            [int_or_404(k.split('_',1)[1]) for k in self.data.keys() if k.startswith('choice')])
        if not self.field_nums:
            if base_num is None:
                base_num = 0
            self.field_nums = [base_num]
        for num in self.field_nums:
            self.fields['choice_%d' % num] = forms.ChoiceField(
                choices=attr_list, widget=forms.Select(attrs={'class':'attr_select'}), required=False)
            # CharField because content is dynamic on the client forms.ChoiceField(choices=())
            self.fields['oper_%d' % num]   = forms.CharField(required=False, widget=forms.Select)
            self.fields['value_%d' % num]  = forms.CharField(required=False)
            self.fields['vvalue_%d' % num] = forms.CharField(required=False,
                widget=forms.Select(attrs={'class':'hidden'}))
        if data is not None:
            for fname in data.keys():
                if fname[:7] == 'choice_' and data[fname]:
                    num = int(fname.split('_',1)[1])
                    dtype = data[fname][0] # First char of T_ACCENAME
                    if dtype not in self.oper_type_map:
                        continue
                    # Populate opers
                    self.fields['oper_%d' % num].widget.choices = self.oper_type_map.get(dtype)
                    if dtype in ('B', 'V'):
                        self.fields['value_%d' % num] = forms.IntegerField(required=False)
                        self.fields['value_%d' % num].widget.attrs['class'] = 'hidden'
                        self.fields['vvalue_%d' % num].widget.attrs['class'] = ''
                        # Populate vvalue
                        if dtype == 'B':
                            choices = [('1', 'Vrai'), ('0', 'Faux')]
                        else:
                            attr_name, subattr_name, prefix = self.decode_attr_name(data[fname][2:])
                            if subattr_name:
                                attr_name = subattr_name
                            try:
                                attr = Attribute.objects.get(name=attr_name)
                            except Attribute.DoesNotExist:
                                continue
                            choices = [(v['id'],v['name']) for v in attr.get_vocab()]
                        self.fields['vvalue_%d' % num].widget.choices = choices
                    else:
                        self.fields['vvalue_%d' % num].widget.attrs['class'] = 'hidden'
        return

    def _collect_attributes(self):
        """ Find all searchable attributes, and return a list of tuples in the form:
            [(V_ATTRNAME, "Attr title), (N_ATTR__SUBATTR, "Attr > Subattr"), ...]
        """
        datatype_map = DatatypeChoices.as_searchtp_dict()
        attr_list = []
        for attr in self.obj_type.get_attributes():
            if not attr.searchable or (self.user.is_anonymous and attr.private):
                continue
            if attr.datatype == 'qset':
                m = re.search(r'''obj_type__code=["'](\w*)["']''', attr.queryset)
                if not m:
                    continue
                subtype_code = m.groups()[0]
                subtype = ObjectType.objects.get(code=subtype_code)
                for subattr in subtype.get_attributes():
                    attr_list.append(
                        ("%s_%s__%s" % (
                            datatype_map.get(subattr.get_datatype()), attr.name, subattr.name),
                         "%s → %s" % (attr.atitle, subattr.atitle)))
            attr_list.append(attr)
        # Collect attributes from possible parent(s)
        parent_types = ObjectType.objects.filter(can_contain=self.obj_type)
        afos = AttributeForObject.objects.filter(obj_type__in=parent_types, searchable_in_child=True
            ).select_related('attr', 'obj_type')
        for afo in afos:
            attr_list.append(
                ("%s_..%s" % (
                    datatype_map.get(afo.attr.get_datatype()), afo.attr.name),
                 "%s → %s" % (afo.obj_type.tname, afo.attr.atitle)))
        # Transform Attribute instances in tuples like (PREF_ATTRIBUTE, verbose_name)
        attr_list2 = []
        for attr in attr_list:
            if isinstance(attr, Attribute):
                attr_list2.append(
                    ("%s_%s" % (datatype_map.get(attr.get_datatype()), attr.name), attr.atitle))
            else:
                attr_list2.append(attr) # already formatted
        return attr_list2

    def clean(self):
        if self.errors:
            return self.cleaned_data
        # At least one choice_ should be set up
        cleaned_data = self.cleaned_data
        choices = filter(lambda x: x[0].startswith('choice_') and x[1], cleaned_data.items())
        values  = filter(lambda x: (x[0].startswith('value_') or x[0].startswith('vvalue_')) and x[1] != '', cleaned_data.items())
        if not choices or not values:
            raise forms.ValidationError(_("You have to choose at least one attribute with a value for searching"))
        for key, val in choices:
            # Check all opers are present
            if not cleaned_data['oper_%s' % key.split('_')[-1]]:
                raise forms.ValidationError(_("One search operator is missing"))
        return cleaned_data

    def _base_query(self):
        # public or (private and user=owner) or (permission group in (list of user groups))
        ugroups = [g.id for g in self.user.groups.all()]
        # TODO: add possibility to limit search to an inventory (with mptt get_descendants() query)
        query = BaseObject.objects.filter(
            Q(permissions__group__pk__in=ugroups) | Q(status="public") |
            Q(status="private", owner__pk=self.user.id)
        )
        if self.user.is_anonymous:
            # Exclure souches mortes pour le public
            query = query.exclude(
                objectvalue__in=ObjectValue.objects.filter(
                    attr__name='ACCSTATUS', value_voc__value_code='0'
                )
            )
        return query

    def search(self):
        query = self._base_query().filter(obj_type=self.obj_type)
        for num in self.field_nums:
            if not self.cleaned_data['choice_%d' % num]:
                continue
            dtype, attr_name = self.cleaned_data['choice_%d' % num].split('_', 1)
            value = self.cleaned_data['vvalue_%d' % num] if dtype in ('B', 'V') else self.cleaned_data['value_%d' % num]
            if value == '':
                continue
            operator = str(self.cleaned_data['oper_%d' % num])
            if attr_name == "TAXONOMY":
                query_filter = Q(**{'taxon__name__%s' % operator: value})
                attr = Attribute.objects.get(name='REMARKS_TAXO')
                query_filter |= Q(**{'objectvalue__attr__pk': attr.id,
                                     'objectvalue__value_text__%s' % operator: value})
                query = query.filter(query_filter)
            elif attr_name.startswith('OQE_'):
                query_filter = Q(**{
                    'mesureparcelle__mesure__%s__%s' % (attr_name[4:].lower(), operator): value
                })
                query = query.filter(query_filter)
            else:
                attr_name, subattr_name, prefix = self.decode_attr_name(attr_name)
                attr = Attribute.objects.get(name=attr_name)
                if subattr_name:
                    # Search one level deeper (object -> value -> object -> value) with a subquery
                    subattr = Attribute.objects.get(name=subattr_name)
                    subquery = eval(attr.queryset)
                    subquery_dict = {'%sobjectvalue__attr__pk' % prefix: subattr.id,
                                     '%sobjectvalue__%s__%s' % (prefix, subattr.get_value_field(), operator): value}
                    subquery = subquery.filter(**subquery_dict)
                    query_dict = {'%sobjectvalue__attr__pk' % prefix: attr.id,
                                  '%sobjectvalue__value_int__in' % prefix: subquery}
                else:
                    query_dict = {'%sobjectvalue__attr__pk' % prefix: attr.id,
                                  '%sobjectvalue__%s__%s' % (prefix, attr.get_value_field(), operator): value}
                query = query.filter(**query_dict)
        text = self.cleaned_data.get('text')
        if text:
            query = query.filter(search_vector=SearchQuery(text))
        results = query.distinct()
        return results

    def next_num(self):
        return self.field_nums[-1] + 1

    def criterion(self):
        """ Used in template to render criterion lines """
        for num in self.field_nums:
            yield (self['choice_%d' % num], self['oper_%d' % num], self['value_%d' % num], self['vvalue_%d' % num])


class ImageSearchForm(SearchForm):
    obj_type_code = "image"


class SearchExportOptionsForm(forms.Form):
    # Dictionary with header labels & column widths for Attribute object name
    EXPORT_FIELDS_DICT = {
        "ACCENUMB": ("Accession number", 12),
        "COLLDATE": ("Collecting date of sample", 14),
        "HOSTDESCR": ("Host Plant", 28),
        "OTHER_SUBSTRATE": ("Other substrate", 28),
    }
    INITIAL_ATTRIBUTES = [
        "ACCENUMB",
        "COLLDATE",
        "HOSTDESCR",
        "OTHER_SUBSTRATE",
    ]

    attributes = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple())

    def __init__(self, attributes_by_type, *args, **kwargs):
        self._initial_attributes_ids = None
        self.attributes_by_type = attributes_by_type
        super().__init__(*args, **kwargs)
        self.fields["attributes"].choices = self.attributes_choices
        self.fields["attributes"].initial = self.initial_attribute_ids

    @property
    def attributes_choices(self):
        choices = []
        for type, attributes in self.attributes_by_type.items():
            for att in attributes:
                choices.append(
                    (att.id, f"[{type}] {self.get_field_label_name(att.name)}")
                )
        return choices

    @cached_property
    def initial_attribute_ids(self):
        return list(
            Attribute.objects.filter(name__in=self.INITIAL_ATTRIBUTES).values_list(
                "pk", flat=True
            )
        )

    @classmethod
    def get_field_label_name(cls, field_name):
        return (
            cls.EXPORT_FIELDS_DICT[field_name][0]
            if field_name in cls.EXPORT_FIELDS_DICT
            else field_name
        )

    @classmethod
    def get_field_col_width(cls, field_name):
        return (
            cls.EXPORT_FIELDS_DICT[field_name][1]
            if field_name in cls.EXPORT_FIELDS_DICT
            else 28
        )
