from django import template
from django.utils.safestring import mark_safe

register = template.Library()

@register.filter
def ident_perc(hsp):
    return hsp.align_length > 0 and round(hsp.identities / hsp.align_length * 100, 2) or 0

@register.filter
def split_align(hsp, step):
    code = ""
    step = int(step)
    for base in range(0, hsp.align_length, step):
        code = "%s<tr><td><pre>%s\n\n%s</pre></td>" % (code, hsp.query_start + base, hsp.sbjct_start + base)
        code = "%s<td><pre>%s\n%s\n%s</pre></td>" % (code, hsp.query[base:base+step], hsp.match[base:base+step], hsp.sbjct[base:base+step])
        code = "%s<td></td></tr>" % (code,)
    return mark_safe("<table>%s</table>" % code)
