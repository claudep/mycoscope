from django.contrib import admin
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from .models import Publication, PressCorner


class PublishingFilter(admin.SimpleListFilter):
    title = _("Publishing")
    parameter_name = "publishing"

    def lookups(self, request, model_admin):
        return [
            ("published", _("already published")),
            ("later-publications", _("later publications")),
            ("not-published", _("not published")),
        ]

    def queryset(self, request, queryset):
        if self.value() == "published":
            return queryset.filter(publishing_datetime__lte=timezone.now())
        elif self.value() == "later-publications":
            return queryset.filter(publishing_datetime__gt=timezone.now())
        elif self.value() == "not-published":
            return queryset.filter(publishing_datetime__isnull=True)
        return queryset


@admin.register(Publication, PressCorner)
class PublicationAdmin(admin.ModelAdmin):
    list_display = ["title", "publishing_datetime"]
    list_filter = [PublishingFilter]
