from django import template
     
register = template.Library()

class AttributeNode(template.Node):
    """ Render the Value object in the context['value'] parameter """
    def render(self, context):
        val = context.get("value")
        return val.render(context)

@register.tag
def render_attr(parser, token):
    return AttributeNode()
