from django.test import TestCase
from django.urls import reverse


class ProjTestCase(TestCase):

    def xtest_register(self):
        response = self.client.get(reverse('register'))
        # Registration is currently disabled by default
        self.assertEqual(response.status_code, 404)
