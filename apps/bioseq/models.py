from io import StringIO
from time import gmtime, strftime

from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.base import ContentFile
from django.contrib.auth.models import User
from django.utils.translation import gettext as _
from Bio import SeqIO, SeqFeature as BioSeqFeature
from Bio.Seq import Seq, UnknownSeq
from Bio.SeqFeature import CompoundLocation, FeatureLocation
from Bio.SeqRecord import SeqRecord
from Bio.SeqUtils.CheckSum import crc64

from taxo.models import Taxon
from invent.models import BaseObject

# Partial mapping of BioSQL structure
"""  Still missing from the BioSQL 1.0 schema:
        taxon,  taxon_name (using taxo.models.Taxon temporarily ?)
        term_synonym, term_dbxref, term_relationship, term_relationship_term
        bioentry_relationship, 
        bioentry_path, term_path
        dbxref_qualifier_value
        seqfeature_relationship, seqfeature_path
"""
# GenBank format is described here:
#    http://www.ncbi.nlm.nih.gov/Sitemap/samplerecord.html

class BioDatabase(models.Model):
    biodatabase_id = models.AutoField(primary_key=True)
    name           = models.CharField(max_length=128, unique=True)
    authority      = models.CharField(max_length=128, blank=True, null=True, db_index=True)
    description    = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "biodatabase"    

    def __str__(self):
        return self.name

    def load(self, records, **kwargs):
        """ Save a parsed content in the database 
            records is an iterable of SeqRecord """
        bioentries = []
        for rec in records:
            be = BioEntry.new_from_seqrecord(rec, self)
            be.set_date_from_record(rec)
            be.set_sequence_from_record(rec)
            be.set_comment_from_record(rec)
            be.set_dbxref_from_record(rec)
            be.set_reference_from_record(rec)
            be.set_annotations_from_record(rec)
            be.set_features_from_record(rec)
            bioentries.append(be)
        return bioentries

    def lookup(self, **kwargs):
        """ Find a bioentry with query parameter """
        query_map = {'display_id':'name', 'primary_id': 'identifier'}
        kwargs['biodatabase'] = self
        # 'Translate' parameter name if necessary (e.g. kwargs['display_id'] -> kwargs['name']
        for key in kwargs:
            if key in query_map:
                kwargs[query_map[key]] = kwargs[key]
                del kwargs[key]
        try:
            be = BioEntry.objects.get(**kwargs)
        except BioEntry.DoesNotExist:
            raise IndexError("No matching BioEntry")
        return DBSeqRecord(be)

    def values(self):
        return [DBSeqRecord(be) for be in BioEntry.objects.filter(biodatabase=self)]

class BioEntry(models.Model):
    """ Note that molecule type and (linear|circular) appearing in LOCUS line of GenBank format is
        currently not recorded in SeqRecords (see http://bugzilla.open-bio.org/show_bug.cgi?id=2578) """
    bioentry_id = models.AutoField(primary_key=True)
    biodatabase = models.ForeignKey(BioDatabase, db_index=True, on_delete=models.CASCADE)
    # Link to own taxon class (might cause some problems as BioSQL taxon class is somewhat different)
    taxon = models.ForeignKey(Taxon, null=True, db_index=True, on_delete=models.SET_NULL)
    # Link to BCIS BaseObject
    baseobjects    = models.ManyToManyField(BaseObject, blank=True)
    name           = models.CharField(max_length=40, db_index=True)
    accession      = models.CharField(max_length=128)
    identifier     = models.CharField(max_length=40, blank=True, null=True)
    unite_number = models.CharField(max_length=25, blank=True)
    # see http://www.ncbi.nlm.nih.gov/Sitemap/samplerecord.html#GenBankDivisionB
    division       = models.CharField(max_length=6, blank=True, null=True)
    description    = models.TextField(blank=True, null=True)
    version        = models.IntegerField()
    dbxrefs        = models.ManyToManyField('Dbxref', through='BioentryDbxref')
    references     = models.ManyToManyField('Reference', through='BioentryReference')

    class Meta:
        db_table = "bioentry"
        unique_together = (('accession', 'biodatabase', 'version'),
                           ('identifier', 'biodatabase'))

    def __str__(self):
        return self.name

    def __getitem__(self, key):
        self._fill_cache()
        if key not in ('keywords', 'source', 'organism', 'taxonomy'):
            raise AttributeError("Object has no '%s' attribute" % key)
        return self._properties.get(key, '')

    def _fill_cache(self):
        """ Get various values from related tables in minimum requests """
        if not hasattr(self, '_properties'):
            self._properties = {}
            for value in self.bioentryqualifiervalue_set.select_related('term').all().order_by('rank'):
                self._properties.setdefault(value.term.name, []).append(value.value)
            for key in self._properties:
                self._properties[key] = "; ".join(self._properties.get(key, []))

    def last_modif_date(self):
        self._fill_cache()
        return self._properties.get('date_changed', _("Not set"))

    def get_sequence(self):
        """ Returns either a DBSeq, an UnknownSeq object or None """
        try:
            seq = self.biosequence.seq
        except ObjectDoesNotExist:
            return None
        if seq is None:
            return UnknownSeq(self.biosequence.length)
        else:
            return DBSeq(self.biosequence)

    def formatted_sequence(self):
        """ Return a sequence formatted in lines of 6 x 10 bases """
        seq = self.get_sequence()
        return seq.format() if seq else ''

    def format(self, format_):
        """ Return the bioentry formatted either in GenBank or FASTA format """
        assert format_ in ('genbank', 'fasta')
        out_handle = StringIO()
        SeqIO.write([DBSeqRecord(self)], out_handle, format_)
        return out_handle.getvalue()

    @classmethod
    def new_from_seqrecord(cls, record, db):
        """ Create a new BioEntry from an exiting SeqRecord object """
        # Code partially pasted from BioSQL.Loader._load_bioentry_table
        if record.id.count(".") == 1: # try to get a version from the id
            #This assumes the string is something like "XXXXXXXX.123"
            accession, version = record.id.split('.')
            try:
                version = int(version)
            except ValueError:
                accession = record.id
                version = 0
        else: # otherwise just use a version of 0
            accession = record.id
            version = 0
 
        if "accessions" in record.annotations \
          and isinstance(record.annotations["accessions"],list) \
          and record.annotations["accessions"]:
            #Take the first accession (one if there is more than one)
            accession = record.annotations["accessions"][0]

        """
        #Find the taxon id (this is not just the NCBI Taxon ID)
        #NOTE - If the species isn't defined in the taxon table,
        #a new minimal entry is created.
        taxon_id = self._get_taxon_id(record)
        """

        if "gi" in record.annotations:
            identifier = record.annotations["gi"]
        else:
            identifier = record.id

        #Allow description and division to default to NULL as in BioPerl.
        description = getattr(record, 'description', None)
        division = record.annotations.get("data_file_division", None)

        obj = BioEntry(biodatabase=db, name=record.name, accession=accession, identifier=identifier,
                       division=division, description=description, version=version)
        obj.save()
        if getattr(record, 'base_object', None):
            obj.baseobjects.add(record.base_object)
        return obj

    def set_date_from_record(self, record):
        # 2 first lines copied from BioSQL.Loader._load_bioentry_date
        # dates are GenBank style, like: 14-SEP-2000
        date = record.annotations.get("date",
                                      strftime("%d-%b-%Y", gmtime()).upper())
        if isinstance(date, list) : date = date[0]
        term = Term.objects.get(name="date_changed", ontology__name="Annotation Tags")
        qvalue = BioEntryQualifierValue(bioentry=self, term=term, value=date, rank=1)
        qvalue.save()

    def set_sequence_from_record(self, record):
        # Code inspired from BioSQL.Loader._load_biosequence
        if not record.seq:
            return
        if isinstance(record.seq, UnknownSeq):
            seq_str = None
        else:
            seq_str = str(record.seq)

        bs = BioSequence(bioentry=self, length=len(record.seq), seq=seq_str)
        bs.save()

    def set_comment_from_record(self, record):
        comments = record.annotations.get('comment')
        if not comments:
            return
        if not isinstance(comments, list):
            comments = [comments]

        for index, comment in enumerate(comments):
            # ? comment = comment.replace('\n', ' ')
            cm = Comment(bioentry=self, comment_text=comment, rank=index+1)
            cm.save()

    def set_dbxref_from_record(self, record):
        # Copied from BioSQL.Loader._load_dbxref
        for rank, value in enumerate(record.dbxrefs):
            # Split the DB:accession string at first colon.
            # We have to cope with things like:
            # "MGD:MGI:892" (db="MGD", accession="MGI:892")
            # "GO:GO:123" (db="GO", accession="GO:123")
            #
            # Annoyingly I have seen the NCBI use both the style
            # "GO:GO:123" and "GO:123" in different vintages.
            assert value.count("\n")==0
            try:
                db, accession = value.split(':',1)
                db = db.strip()
                accession = accession.strip()
            except:
                raise ValueError("Parsing of dbxrefs list failed: '%s'" % value)

            # Get or create objects
            dbxref, cr = Dbxref.objects.get_or_create(dbname=db, accession=accession, defaults={'version':0})
            dbxref_bioe, cr = BioentryDbxref.objects.get_or_create(dbxref=dbxref, bioentry=self, defaults={'rank': rank+1})

    def set_reference_from_record(self, record):
        references = record.annotations.get('references', ())
        for rank, reference in enumerate(references):
            ref = None
            if reference.medline_id:
                try:
                    ref = Reference.objects.get(dbxref__dbname='MEDLINE', dbxref__accession=reference.medline_id)
                except Reference.DoesNotExist:
                    pass
            if not ref and reference.pubmed_id:
                try:
                    ref = Reference.objects.get(dbxref__dbname='PUBMED', dbxref__accession=reference.pubmed_id)
                except Reference.DoesNotExist:
                    pass
            if not ref:
                crc = crc64("%s%s%s" % (reference.authors or "<undef>", reference.title or "<undef>", reference.journal or "<undef>"))
                try:
                    ref = Reference.objects.get(crc=crc)
                except Reference.DoesNotExist:
                    pass
            if not ref:
                dbxref = None
                if reference.medline_id:
                    dbxref = Dbxref(dbname="MEDLINE", accession=reference.medline_id, version=0)
                    dbxref.save()
                elif reference.pubmed_id:
                    dbxref = Dbxref(dbname="PUBMED", accession=reference.pubmed_id, version=0)
                    dbxref.save()
                authors = reference.authors or None
                title =  reference.title or None
                #The location/journal field cannot be Null, so default
                #to an empty string rather than None:
                journal = reference.journal or ""
                ref = Reference(dbxref=dbxref, location=reference.journal or "",
                                title=reference.title or None, authors=reference.authors or None,
                                crc=crc)
                ref.save()
            if reference.location:
                start = 1 + int(str(reference.location[0].start))
                end = int(str(reference.location[0].end))
            else:
                start = None
                end = None
            ref_bioentry = BioentryReference(bioentry=self, reference=ref,
                                             start_pos=start, end_pos=end, rank=rank+1)
            ref_bioentry.save()

    def set_annotations_from_record(self, record):
        ontology = Ontology.objects.get(name='Annotation Tags')
        for key, value in record.annotations.items():
            if key in ["references", "comment", "ncbi_taxid"]:
                # Handled separately in there own tables
                continue
            term, cr = Term.objects.get_or_create(name=key, ontology=ontology)
            if isinstance(value, (list, tuple)):
                rank = 0
                for entry in value:
                    if isinstance(entry, (str, int)):
                        #Easy case
                        rank += 1
                        qvalue = BioEntryQualifierValue(bioentry=self, term=term, value=entry, rank=rank)
                        qvalue.save()
                    else:
                        pass
                        #print "Ignoring annotation '%s' sub-entry of type '%s'" \
                        #      % (key, str(type(entry)))
            elif isinstance(value, (str, int)):
                #Have a simple single entry, leave rank as the DB default
                qvalue = BioEntryQualifierValue(bioentry=self, term=term, value=value)
                qvalue.save()
            else:
                pass
                #print "Ignoring annotation '%s' entry of type '%s'" \
                #      % (key, type(value))

    def set_features_from_record(self, record):
        key_ontology = Ontology.objects.get(name='SeqFeature Keys')
        src_ontology = Ontology.objects.get(name='SeqFeature Sources')
        tag_ontology = Ontology.objects.get(name='Annotation Tags')
        for feat_num, feature in enumerate(record.features):
            key_term, cr = Term.objects.get_or_create(name=feature.type, ontology=key_ontology)
            src_term, cr = Term.objects.get_or_create(name='EMBL/GenBank/SwissProt', ontology=src_ontology)
            seq_feat = SeqFeature(bioentry=self, type_term=key_term, source_term=src_term, rank=feat_num+1)
            seq_feat.save()
            # Locations # See bioSQL Loader.py for extensive comments
            for rank, location in enumerate(feature.location.parts):
                dbxref = None
                if location.ref:
                    dbxref, cr = Dbxref.objects.get_or_create(dbname=location.ref_db or "", accession=location.ref, defaults={'version':0})
                loc = Location(seqfeature=seq_feat, dbxref=dbxref,
                               start_pos=int(location.start) + 1, end_pos=int(location.end),
                               strand=location.strand, rank=rank+1)
                loc.save()
            # Qualifiers
            for key, qualifier in feature.qualifiers.items():
                entries = isinstance(qualifier, list) and qualifier or [qualifier]
                if key != 'db_xref':
                    qual_term, cr = Term.objects.get_or_create(name=key, ontology=tag_ontology)
                    for rank, entry in enumerate(entries):
                        qvalue = SeqFeatureQualifierValue(seqfeature=seq_feat, term=qual_term, rank=rank+1, value=entry)
                        qvalue.save()
                else:
                    for rank, value in enumerate(entries):
                        # Split the DB:accession format string at colons.  We have to
                        # account for multiple-line and multiple-accession entries
                        try:
                            dbxref_data = value.replace(' ','').replace('\n','').split(':')
                            db = dbxref_data[0]
                            accessions = dbxref_data[1:]
                        except:
                            raise ValueError("Parsing of db_xref failed: '%s'" % value)
                        for accession in accessions:
                            dbxref, cr = Dbxref.objects.get_or_create(dbname=db, accession=accession, defaults={'version':0})
                            seqf_dbxref, cr = SeqFeatureDbxref.objects.get_or_create(seqfeature=seq_feat, dbxref=dbxref, rank=rank+1)


class BioSequence(models.Model):
    bioentry = models.OneToOneField(BioEntry, primary_key=True, on_delete=models.CASCADE)
    version     = models.IntegerField(blank=True, null=True)
    length      = models.IntegerField(blank=True, null=True)
    alphabet    = models.CharField(max_length=10, blank=True, null=True)
    seq         = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "biosequence"

    def blastn(self, user, mode):
        """ 'mode' is either 'local' or 'www' (online blast) """
        if mode == 'local':
            from bioseq.utils import blastn
            res = blastn(self.seq)
            res_handle = ContentFile(res[1])
            cmd = res[0]
        elif mode == 'www':
            from Bio.Blast import NCBIWWW
            res = NCBIWWW.qblast("blastn", "nt", self.bioentry.format('fasta'))
            res_handle = ContentFile(res.read())
            cmd = "https://blast.ncbi.nlm.nih.gov/Blast.cgi"
        else:
            raise ValueError("Unknown blast mode '%s'" % mode)

        blast = BlastResult(blast_cmd=cmd, blast_who=user, bioentry=self.bioentry)
        blast.result_file.save("blast_of_%s.xml" % strftime("%d-%b-%Y_%H-%M-%S"), res_handle)
        return blast


class BlastResult(models.Model):
    """ Archived blast results (not in standard bio DB) """
    blast_cmd   = models.CharField(max_length=250)
    blast_date  = models.DateTimeField(auto_now_add=True)
    blast_who = models.ForeignKey(User, on_delete=models.PROTECT)
    bioentry = models.ForeignKey(BioEntry, on_delete=models.CASCADE)
    result_file = models.FileField(upload_to="blasts")

    class Meta:
        db_table = "blastfile"
        ordering = ['blast_date']

    def __str__(self):
        return "%s by %s on %s" % (self.result_file, self.blast_who, self.blast_date)

    def read(self):
        from Bio.Blast import NCBIXML
        self.result_file.open('r')  # Force open in text mode
        return NCBIXML.read(self.result_file.file)

## Ontology terms and relationships

class OntologyManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)

class Ontology(models.Model):
    ontology_id = models.AutoField(primary_key=True)
    name        = models.CharField(max_length=32, unique=True)
    definition  = models.TextField(blank=True, null=True)

    objects = OntologyManager()
    class Meta:
        db_table = "ontology"

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name,)


class Term(models.Model):
    term_id     = models.AutoField(primary_key=True)
    name        = models.CharField(max_length=255)
    definition  = models.TextField(blank=True, null=True)
    identifier  = models.CharField(max_length=40, unique=True, blank=True, null=True)
    is_obsolete = models.CharField(max_length=1, blank=True, null=True)
    ontology = models.ForeignKey(Ontology, on_delete=models.PROTECT)

    class Meta:
        db_table = "term"
        unique_together = ('name', 'ontology', 'is_obsolete')

    def __str__(self):
        return self.name


class BioEntryQualifierValue(models.Model):
    """ tag/value and ontology term annotation for bioentries goes here """
    # Unlike BioSQL 1.0 schema, this table has an 'id' primary key
    bioentry = models.ForeignKey(BioEntry, on_delete=models.CASCADE)
    term = models.ForeignKey(Term, on_delete=models.CASCADE)
    value = models.TextField(blank=True, null=True)
    rank = models.IntegerField(default=0)

    class Meta:
        db_table = "bioentry_qualifier_value"
        unique_together = ('bioentry', 'term', 'rank')

    def __str__(self):
        return "%s: %s (for %s)" % (self.term.name, self.value, self.bioentry.name)


class Comment(models.Model):
    comment_id   = models.AutoField(primary_key=True)
    bioentry     = models.ForeignKey(BioEntry, on_delete=models.CASCADE)
    comment_text = models.TextField()
    rank         = models.IntegerField(default=0)

    class Meta:
        db_table = "comment"
        unique_together = ('bioentry', 'rank')


class Dbxref(models.Model):
    dbxref_id   = models.AutoField(primary_key=True)
    dbname      = models.CharField(max_length=40, db_index=True)
    accession   = models.CharField(max_length=128)
    version     = models.IntegerField()

    class Meta:
        db_table = "dbxref"
        unique_together = ('accession', 'dbname', 'version')

    def __str__(self):
        """ Returns a string like MGD:MGI:892 """
        if self.version and self.version != "0":
            v = "%s.%s" % (self.accession, self.version)
        else:
            v = self.accession
        return "%s:%s" % (self.dbname, v)

    def as_tuple(self):
        """ Return a tuple (dbname, accession+version) """
        if self.version and self.version != "0":
            v = "%s.%s" % (self.accession, self.version)
        else:
            v = self.accession
        dbname = self.dbname or None
        return (dbname, v)


class BioentryDbxref(models.Model):
    """ Link table between BioEntry and Dbxref """
    # Unlike BioSQL 1.0 schema, this table has an 'id' primary key
    bioentry = models.ForeignKey(BioEntry, on_delete=models.CASCADE)
    dbxref   = models.ForeignKey(Dbxref, on_delete=models.CASCADE)
    rank     = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = "bioentry_dbxref"
        unique_together = ('bioentry', 'dbxref')


class Reference(models.Model):
    reference_id = models.AutoField(primary_key=True)
    dbxref = models.OneToOneField(Dbxref, blank=True, null=True, on_delete=models.CASCADE)
    location     = models.TextField()
    title        = models.TextField(blank=True, null=True)
    authors      = models.TextField(blank=True, null=True)
    crc          = models.CharField(max_length=32, unique=True, blank=True, null=True)

    class Meta:
        db_table = "reference"

    def __str__(self):
        return "%s (%s)" % (self.title, self.authors)


class BioentryReference(models.Model):
    """ Link table between BioEntry and Reference """
    # Unlike BioSQL 1.0 schema, this table has an 'id' primary key
    bioentry = models.ForeignKey(BioEntry, on_delete=models.CASCADE)
    reference = models.ForeignKey(Reference, on_delete=models.CASCADE)
    start_pos = models.IntegerField(blank=True, null=True)
    end_pos   = models.IntegerField(blank=True, null=True)
    rank      = models.IntegerField(default=0)

    class Meta:
        db_table = "bioentry_reference"
        unique_together = ('bioentry', 'reference', 'rank')

    def get_bioreference(self):
        """ Return the reference as an BioPython SeqFeature.Reference() object """
        reference = BioSeqFeature.Reference()
        if self.start_pos or self.end_pos:
            reference.location = [FeatureLocation(self.start_pos-1, self.end_pos)]
        #Don't replace the default "" with None.
        if self.reference.authors : reference.authors = self.reference.authors
        if self.reference.title : reference.title = self.reference.title
        reference.journal = self.reference.location
        dbname = self.reference.dbxref and self.reference.dbxref.dbname or None
        if dbname == 'PUBMED':
            reference.pubmed_id = self.reference.dbxref.accession
        elif dbname == 'MEDLINE':
            reference.medline_id = self.reference.dbxref.accession
        return reference


class SeqFeature(models.Model):
    seqfeature_id = models.AutoField(primary_key=True)
    bioentry = models.ForeignKey(BioEntry, on_delete=models.CASCADE)
    type_term = models.ForeignKey(Term, related_name='type_term', on_delete=models.CASCADE)
    source_term = models.ForeignKey(Term, related_name='source_term', on_delete=models.CASCADE)
    display_name  = models.CharField(max_length=64, blank=True, null=True)
    rank          = models.IntegerField(default=0)
    dbxrefs       = models.ManyToManyField('Dbxref', through='SeqFeatureDbxref')

    class Meta:
        db_table = "seqfeature"
        unique_together = ('bioentry', 'type_term', 'source_term', 'rank')

    def location(self):
        """ Return the location of the feature as atuple (start, end) """
        # Only simple span currently supported
        if self.location_set.count() > 1:
            raise NotImplementedError("More than one location by feature is not supported currently")
        try:
            loc = self.location_set.all()[0]
        except IndexError:
            return None
        return {'id': self.pk, 'start': loc.start_pos, 'end': loc.end_pos}

    def qualifiers(self):
        return self.seqfeaturequalifiervalue_set.all().order_by('rank')


class SeqFeatureQualifierValue(models.Model):
    # Unlike BioSQL 1.0 schema, this table has an 'id' primary key
    seqfeature = models.ForeignKey(SeqFeature, on_delete=models.CASCADE)
    term = models.ForeignKey(Term, on_delete=models.CASCADE)
    rank = models.IntegerField(default=0)
    value = models.TextField()

    class Meta:
        db_table = "seqfeature_qualifier_value"
        unique_together = ('seqfeature', 'term', 'rank')

    def __str__(self):
        return "%s: %s" % (self.term, self.value)


class SeqFeatureDbxref(models.Model):
    # Unlike BioSQL 1.0 schema, this table has an 'id' primary key
    seqfeature = models.ForeignKey(SeqFeature, on_delete=models.CASCADE)
    dbxref = models.ForeignKey(Dbxref, on_delete=models.CASCADE)
    rank = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = "seqfeature_dbxref"
        unique_together = ('seqfeature', 'dbxref')


class Location(models.Model):
    location_id = models.AutoField(primary_key=True)
    seqfeature = models.ForeignKey(SeqFeature, on_delete=models.CASCADE)
    dbxref = models.ForeignKey(Dbxref, blank=True, null=True, on_delete=models.CASCADE)
    term = models.ForeignKey(Term, blank=True, null=True, on_delete=models.CASCADE)
    # Start_pos is stored 1-based, like in file formats, but should be passed to
    # biopython as 0-based (currently done in the code, should be a custom field?)
    start_pos   = models.IntegerField(blank=True, null=True)
    end_pos     = models.IntegerField(blank=True, null=True)
    strand      = models.IntegerField(default=0)
    rank        = models.IntegerField(default=0)

    class Meta:
        db_table = "location"
        unique_together = ('seqfeature', 'rank')

    def __str__(self):
        return "%d..%d" % (self.start_pos, self.end_pos)

    def __init__(self, *args, **kwargs):
        super(Location, self).__init__(*args, **kwargs)
        if self.strand == 0: self.strand = None

    def save(self, **kwargs):
        if self.strand == None: self.strand = 0
        super(Location, self).save(**kwargs)

    def get_locationqvalue(self):
        try:
             self.locationqualifiervalue_set.all()[0].value
        except IndexError:
            return ""

    def get_remote_ref(self):
        if self.dbxref:
            return self.dbxref.as_tuple()
        else:
            return (None, None)


class LocationQualifierValue(models.Model):
    # Unlike BioSQL 1.0 schema, this table has an 'id' primary key
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    term = models.ForeignKey(Term, on_delete=models.CASCADE)
    value     = models.CharField(max_length=255)
    int_value = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = "location_qualifier_value"
        unique_together = ('location', 'term')

# Subclasses of standard BioPython classes.
#   - DBSeqRecord -> SeqRecord

class DBSeqRecord(SeqRecord):
    def __init__(self, bioentry):
        self.bioentry = bioentry
        if bioentry.version and bioentry.version != "0":
            id = "%s.%s" % (bioentry.accession, bioentry.version)
        else:
            id = bioentry.accession
        # Retrieve features
        features = []
        annotations = {}
        for db_feature in self.bioentry.seqfeature_set.all().order_by('rank'):
            feature = BioSeqFeature.SeqFeature(type = db_feature.type_term.name)
            feature.qualifiers = {}
            for qvalue in db_feature.seqfeaturequalifiervalue_set.select_related('term').order_by('rank'):
                feature.qualifiers.setdefault(qvalue.term.name, []).append(qvalue.value)
            for xref in db_feature.dbxrefs.all().order_by('seqfeaturedbxref__rank'):
                feature.qualifiers.setdefault("db_xref", []).append("%s:%s" % (xref.dbname, xref.accession))
            loc_num = db_feature.location_set.count()
            if loc_num == 1:
                loc = db_feature.location_set.select_related('dbxref')[0]
                feature.location_operator = loc.get_locationqvalue()
                feature.location = FeatureLocation(loc.start_pos-1, loc.end_pos)
                feature.strand = loc.strand
                feature.ref_db, feature.ref = loc.get_remote_ref()
            elif loc_num > 1:
                feature_start = None
                locations = []
                for loc in db_feature.location_set.select_related('dbxref').order_by('rank'):
                    new_loc = FeatureLocation(
                        loc.start_pos-1, loc.end_pos, strand=loc.strand,
                    )
                    new_loc.ref_db, new_loc.ref = loc.get_remote_ref()
                    locations.append(new_loc)
                feature.location = CompoundLocation(locations)
            features.append(feature)

        qvalues = bioentry.bioentryqualifiervalue_set.select_related('term').order_by('rank').values('term__name', 'value')
        for qval in qvalues:
            key = {'keyword': 'keywords', 'date_changed': 'dates', 'secondary_accession': 'accessions'
                  }.get(qval['term__name'], qval['term__name'])
            annotations.setdefault(key, []).append(qval['value'])
        references = []
        for bioref in bioentry.bioentryreference_set.select_related('bioentry', 'reference', 'reference__dbxref').all().order_by('rank'):
            references.append(bioref.get_bioreference())
        if references:
            annotations['references'] = references
        for com in bioentry.comment_set.order_by('rank'):
            annotations.setdefault('comment', []).append(com.comment_text)
        # Transform single-item lists as strings
        for k, v in annotations.items():
            if isinstance(v, list) and len(v) == 1 and isinstance(v[0], str) and k != 'keywords':
                annotations[k] = v[0]

        # TODO: annotations.update(_retrieve_taxon(adaptor, primary_id, taxon_id))"""
        if 'date' in annotations:
            annotations['date'] = str(annotations['date']) # Workaround for http://bugzilla.open-bio.org/show_bug.cgi?id=3118
        super().__init__(bioentry.get_sequence(),
                         name = bioentry.name, id = id, description=bioentry.description,
                         dbxrefs = [str(xref) for xref in self.bioentry.dbxrefs.all()],
                         features = features, annotations = annotations)

class DBSeq(Seq):
    def __init__(self, bioseq):
        self.biosequence = bioseq
        super().__init__(bioseq.seq)

    def __str__(self):
        """Returns the full sequence as a python string """
        return self.biosequence.seq or ""

    def __getitem__(self, index):
        if isinstance(index, int):
            #Return a single letter as a string
            return self.biosequence.seq[index]
        else:
            #Return the (sub)sequence as another Seq object
            return Seq(self.biosequence.seq[index])

    def __add__(self, other):
        #Let the Seq object deal with the alphabet issues etc
        return self.toseq() + other

    def __radd__(self, other):
        #Let the Seq object deal with the alphabet issues etc
        return other + self.toseq()

    @property
    def data(self):
        return str(self)

    def toseq(self):
        """Returns the full sequence as a Seq object."""
        #Note - the method name copies that of the MutableSeq object
        return Seq(str(self))

    def format(self):
        """ Format the sequence as 6 groups of 10 bases per line """
        def group(s, n):
             return [s[i:i+n] for i in range(0, len(s), n)]
        data = str(self)
        lines = []
        for i, line in enumerate(group(data, 60)):
            lines.append((str(i*60+1).rjust(4), group(line, 10)))
        return lines

# ************  Signals ******************
from django.db.models.signals import post_save
from bioseq.utils import make_blast_db

def update_blast_db(sender, instance, **kwargs):
    make_blast_db()

post_save.connect(update_blast_db, sender=BioSequence)
