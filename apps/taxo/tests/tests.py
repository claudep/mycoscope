from django.test import TestCase
from django.test.client import Client
from django.urls import reverse

from taxo.models import Taxon


class TaxoTestCase(TestCase):
    def setUp(self):
        tx_data = {'FAMILY': 'Betulaceae', 'GENUS': 'Betula', 'SPECIES': 'pendula'}
        self.tx = Taxon.get_from_data(tx_data, create_if_absent=True)

    def testCreateTaxonThroughGet(self):
        """ This test creation of taxon when trying to get one that doesn't exist """
        taxon_data = {'FAMILY': 'Vitaceae', 'GENUS': 'Vitis', 'SPECIES': 'vinifera' }
        tx = Taxon.get_from_data(taxon_data, create_if_absent=True)
        self.assertEqual(tx.name, 'vinifera')
        self.assertEqual(tx.rank, 'species')
        # Add also author information
        taxon_data2 = {'FAMILY': 'Violaceae', 'GENUS': 'Viola', 'GENUS_AUTHOR': 'Me', 'SPECIES': 'tricolor', 'SPAUTHOR': 'L.', 'SUBSPECIES': 'claris', 'SSPAUTHOR':'Cool guy' }
        tx = Taxon.get_from_data(taxon_data2, create_if_absent=True)
        self.assertEqual(tx.author, 'Cool guy')
        self.assertEqual(tx.parent.author, 'L.')
        self.assertEqual(tx.get_taxon_by_rank('genus').author, 'Me')

    def testTaxonURL(self):
        absurl = self.tx.get_absolute_url()
        self.assertTrue(absurl.startswith('/') and absurl.endswith('/'))

    def testTaxonBrowsingByRank(self):
        """ Test that taxonomy browse by specifiyng a rank is working """
        browse_url = reverse("taxo_tree_by_rank", args=['familia', 'select'])
        c = Client()
        response = c.get(browse_url)
        self.assertContains(response, "Betulaceae")

    def testTaxonBrowsingByTaxon(self):
        """ Test that taxonomy browse by specifiyng a taxon is working """
        browse_url = reverse("taxo_browse", args=[self.tx.parent.id])
        c = Client()
        response = c.get(browse_url)
        self.assertContains(response, "Betulaceae")
        self.assertContains(response, "Betula")
        self.assertContains(response, "pendula")

    def testGettingParentList(self):
        """ Test taxon.get_parents method """
        par = self.tx.get_parents()
        self.assertEqual(len(par), 3)
        self.assertEqual(par[0].name, "Betulaceae")
        self.assertEqual(par[1].name, "Betula")
        self.assertEqual(par[2].name, "pendula")
        par = self.tx.get_parents(stop_rank_name='genus')
        self.assertEqual(len(par), 2)
        self.assertEqual(par[0].name, "Betula")
        self.assertEqual(par[1].name, "pendula")
