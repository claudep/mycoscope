from django.test import TestCase
from django.contrib.auth.models import User, Group, AnonymousUser

from invent.data import data
from invent.models import BaseObject, ObjectType

class PermissionTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('john', 'doe@example.org', 'johnpw')
        self.user2 = User.objects.create_user('john2', 'doe2@example.org', 'johnpw')
        self.group = Group(name='mygroup')
        self.group.save()
        data.load_object_types()
        self.spec_type = ObjectType.objects.get(code="specimen")
        self.list_type = ObjectType.objects.get(code="inventory")
        self.obj = self._create_object(self.spec_type)

    def _create_object(self, o_type, data={}):
        """ Create a BasicObject """
        obj = BaseObject(
            title      = data.get('title', u"Test obj"),
            obj_type   = o_type,
            description= data.get('description', u"Description text."),
            owner      = data.get('owner', self.user),
            status     = data.get('status', 'public'),
            parent     = data.get('parent', None),
        )
        obj.save()
        if 'values' in data:
            for key, val in data['values']:
                obj.set_value_for(key, val)
        return obj

    def testReadPermOnPublicObject(self):
        self.obj.status = 'public'
        self.obj.save()
        self.assertTrue(self.obj.has_read_permission(self.user))
        self.assertTrue(self.obj.has_read_permission(AnonymousUser()))

    def testReadPermOnPrivateObject(self):
        self.obj.status = 'private'
        self.obj.save()
        self.assertTrue(self.obj.has_read_permission(self.user))
        self.assertFalse(self.obj.has_read_permission(AnonymousUser()))
        # Check that read permission is propagated through group membership
        self.assertFalse(self.obj.has_read_permission(self.user2))
        self.user2.groups.add(self.group)
        self.obj.add_read_permission(self.group)
        self.assertTrue(self.obj.has_read_permission(self.user2))

    def testApplyPermOnChildren(self):
        """ Test permission applied to parent with apply_to_children=True """
        lst = self._create_object(self.list_type)
        obj = self._create_object(self.spec_type, {'parent': lst, 'status':'private', 'title':'Obj1'})
        self.user2.groups.add(self.group)
        lst = BaseObject.objects.get(id=lst.id) # refresh object
        lst.add_read_permission(self.group, apply_to_children=True)
        self.assertTrue(obj.has_read_permission(self.user2))
        # The same should happen for objects created *after* (herit at creation)
        obj2 = self._create_object(self.spec_type, {'parent': lst, 'status':'private', 'title':'Obj2'})
        self.assertTrue(obj2.has_read_permission(self.user2))

    def testMultiplePermApply(self):
        """ Test that applying mutiple time a permission to an object doesn't create more than one Permission instance by group/object """
        self.obj.add_read_permission(self.group, apply_to_children=True)
        self.obj.add_write_permission(self.group, apply_to_children=True)
        self.obj.add_read_permission(self.group, apply_to_children=True)
        self.obj = BaseObject.objects.get(id=self.obj.id) # refresh object
        self.assertEqual(len(self.obj.permissions.all()), 1)

