from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin
from django.contrib.auth.models import Group

from invent import models


class BaseObjectAdmin(admin.ModelAdmin):
    list_filter = ('obj_type',)
    list_display = ('title_not_empty', 'weight')
    search_fields = ['title', 'description']
    raw_id_fields = ('taxon','parent')

    def title_not_empty(self, obj):
        return obj.title or '<no title>'
    title_not_empty.short_description = "Title"


class AttributeAdmin(admin.ModelAdmin):
    search_fields = ['name', 'ahelp']
    list_display = ('name', 'atitle', 'active', 'private', 'searchable', 'datatype', 'vocab')
    list_filter = ('datatype',)
#    raw_id_fields = ('parent',)

class AttributeForObjectInline(admin.TabularInline):
    model = models.AttributeForObject

class ObjectTypeAdmin(admin.ModelAdmin):
    inlines = [ AttributeForObjectInline ]

class AttributeForObjectAdmin(admin.ModelAdmin):
    list_display = ('obj_type', 'attr_group', 'attr', 'position')

class AttributeInGroupInline(admin.TabularInline):
    model = models.AttributeInGroup
    ordering = ('position',)

class AttributeGroupAdmin(admin.ModelAdmin):
    list_display = ('gname', 'category', 'position')
    inlines = [ AttributeInGroupInline ]

class VocabValueAdmin(admin.ModelAdmin):
    search_fields = ('value_code', 'value')

class VocabValueInline(admin.TabularInline):
    model = models.VocabValue

class VocabularyAdmin(admin.ModelAdmin):
    inlines = [ VocabValueInline ]


class ObjectValueAdmin(admin.ModelAdmin):
    search_fields = ['attr__name', 'value_text', 'value_int', 'value_float']
    list_filter = ['attr']


class MyGroupAdmin(GroupAdmin):
    list_display = list(GroupAdmin.list_display) + ['get_users']

    def get_users(self, grp):
        return ', '.join(grp.user_set.values_list('username', flat=True))

admin.site.unregister(Group)
admin.site.register(Group, MyGroupAdmin)

admin.site.register(models.ObjectType, ObjectTypeAdmin)
admin.site.register(models.BaseObject, BaseObjectAdmin)
admin.site.register(models.ObjectValue, ObjectValueAdmin)
admin.site.register(models.ObjectValueStats)
admin.site.register(models.Attribute, AttributeAdmin)
admin.site.register(models.AttributeGroup, AttributeGroupAdmin)
admin.site.register(models.AttributeForObject, AttributeForObjectAdmin)
admin.site.register(models.Vocabulary, VocabularyAdmin)
admin.site.register(models.VocabValue, VocabValueAdmin)
