from django.contrib.auth.models import User, Group
from django.urls import reverse

from invent import models
from invent import forms
from invent.tests.base import BaseTestCase, test_in_english


class SearchTests(BaseTestCase):

    @test_in_english
    def test_display_search(self):
        response = self.client.get(reverse('search'))
        self.assertContains(
            response,
            '<option value="T_TAXONOMY">Taxonomy</option>',
            html=True
        )

    def test_add_criterion(self):
        """ Test ajax call to dynamically add criterion to search form """
        url = reverse('add_criterion', args=[1])
        response = self.client.get(url)
        self.assertContains(response, '<option value="N_ATTR_INT">Test integer attribute</option>', html=True)
        self.assertContains(response, '<select name="oper_1" id="id_oper_1"></select>', html=True)
        self.assertContains(response, '<input type="text" name="value_1" id="id_value_1" />', html=True)

    def test_getting_vocab(self):
        """ Test ajax call to retrieve vocab for an attribute """
        response = self.client.get(reverse('vocab_for_attribute'), data={'attr_name': 'ATTR_VOCAB'})
        self.assertContains(response, "oui")
        # Test when attribute is subattribute
        response = self.client.get(reverse('vocab_for_attribute'),
            data={'attr_name': 'ATTR_QUERYSET__ATTR_VOCAB'})
        self.assertContains(response, "oui")

    def test_criteria_search(self):
        new_obj1 = self._create_object(
            self.spec_type,
            title="An object", values=(('ATTR_TEXT','An accession'),('ATTR_VOCAB','yes'),('ATTR_INT','2005')))
        new_obj2 = self._create_object(
            self.spec_type,
            title="Another object", values=(('ATTR_TEXT','Second accession'),('ATTR_VOCAB','no')))
        # Search a numeric value
        f = forms.SearchForm(
            data={'choice_0':'N_ATTR_INT', 'oper_0':'gte', 'value_0': '2005'})
        self.assertTrue(f.is_valid())
        res = f.search()
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0].pk, new_obj1.id)
        # Search with two criteria (text/vocabulary)
        vocval = models.VocabValue.objects.get(vocab__name='YesNo', value_code='no')
        f = forms.SearchForm(data={
            'choice_0':'T_ATTR_TEXT', 'oper_0':'icontains', 'value_0': 'Accession',
            'choice_1':'V_ATTR_VOCAB', 'oper_1':'exact', 'vvalue_1': str(vocval.id)
        })
        self.assertTrue(f.is_valid())
        res = f.search()
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0].pk, new_obj2.id)
        # It may happen that the 'oper' field is not populated, must raise ValidationError
        f = forms.SearchForm(
            data={'choice_0':'T_ACCENAME', 'value_0': '2005'})
        self.assertFalse(f.is_valid())
        # Search with non-consecutive field numbers
        f = forms.SearchForm(data={
            'choice_2':'T_ATTR_TEXT', 'oper_2':'icontains', 'value_2': 'Accession',
            'choice_4':'V_ATTR_VOCAB', 'oper_4':'exact', 'vvalue_4': str(vocval.id)})
        self.assertTrue(f.is_valid())
        res = f.search()
        self.assertEqual(len(res), 1)

    def test_subcriteria_search(self):
        """ Test search of subattributes (attribute that is a 'qset' attribute of another object) """
        models.AttributeForObject.objects.create(
            obj_type=self.spec_type,
            attr=models.Attribute.objects.get(datatype='qset')
        )
        new_obj1 = self._create_object(
            self.spec_type,
            title="A specimen", values=(('ATTR_TEXT','A specimen'),('ATTR_INT',245)))
        new_obj2 = self._create_object(
            self.spec_type,
            title="An other specimen", values=(('ATTR_TEXT','My inventory'), ('ATTR_QUERYSET',new_obj1.pk)))

        f = forms.SearchForm(data={
            'choice_0': 'N_ATTR_QUERYSET__ATTR_INT', 'oper_0': 'lt', 'value_0': 400,
        })
        self.assertTrue(f.is_valid(), f.errors)
        res = f.search()
        self.assertEqual(len(res), 1)

    def test_parentcriteria_search(self):
        new_obj1 = self._create_object(
            self.spec_type,
            title="A specimen", values=(('ATTR_TEXT','A specimen'),('ATTR_INT',245)))
        new_obj2 = self._create_object(
            self.list_type,
            title="An inventory", values=(('ATTR_TEXT','My inventory'),('ATTR_QUERYSET',new_obj1.pk)))
        new_obj1.parent = new_obj2
        new_obj1.save()
        afo, cr = models.AttributeForObject.objects.get_or_create(
            obj_type=self.list_type, attr=models.Attribute.objects.get(name='ATTR_TEXT'))
        afo.searchable_in_child = True
        afo.save()

        f = forms.SearchForm(data={
            'choice_0': 'T_..ATTR_TEXT', 'oper_0': 'icontains', 'value_0': 'inventory',
        })
        self.assertTrue(f.is_valid(), f.errors)
        res = f.search()
        self.assertEqual(len(res), 1)

    def test_taxonomy_search(self):
        """ Test that taxonomy search also search in REMARKS_TAXO """
        models.Attribute.objects.create(name="REMARKS_TAXO", datatype="text", atitle="Taxonomy remarks")
        new_obj1 = self._create_object(self.spec_type,
            title="Object public", status='public',
            values=(('ATTR_TEXT','Object public'),('REMARKS_TAXO', 'Phytophthora')))
        f = forms.SearchForm(data={
            'choice_0':'T_TAXONOMY', 'oper_0':'icontains', 'value_0': 'Phytophthora',
        })
        self.assertTrue(f.is_valid(), f.errors)
        res = f.search()
        self.assertEqual(len(res), 1)

    def test_search_visibility(self):
        """ Test that private/protected objects don't appear in search results """
        # Create public/private/protected objects
        new_obj1 = self._create_object(self.spec_type, title="Object public",
            status='public', values=(('ATTR_TEXT','Object public'),))
        new_obj2 = self._create_object(self.spec_type, title="Object private",
            status='private', values=(('ATTR_TEXT','Object private'),))
        new_obj3 = self._create_object(self.spec_type, title="Object protected",
            status='private', values=(('ATTR_TEXT','Object protected'),))
        user2 = User.objects.create_user('john2', 'doe2@example.org', 'john2pw')
        group = Group(name='mygroup')
        group.save()
        user2.groups.add(group)
        new_obj3.add_read_permission(group)
        search_data = {'choice_0':'T_ATTR_TEXT', 'oper_0':'icontains', 'value_0': 'Object'}
        # Public search should show 1 (public)
        f = forms.SearchForm(data=search_data)
        self.assertTrue(f.is_valid())
        res = f.search()
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0].pk, new_obj1.id)
        # owner search should show 3 (public/private/protected)
        f = forms.SearchForm(data=search_data, user=self.user)
        self.assertTrue(f.is_valid())
        res = f.search()
        self.assertEqual(len(res), 3)
        # other user search should show 2 (public/protected)
        f = forms.SearchForm(data=search_data, user=user2)
        self.assertTrue(f.is_valid())
        res = f.search()
        self.assertEqual(len(res), 2)
        self.assertTrue(res[0].pk in (new_obj1.id, new_obj3.id))
        self.assertTrue(res[1].pk in (new_obj1.id, new_obj3.id))

    def test_search_dead_specimen(self):
        dead_obj = self._create_object(
            self.spec_type, title="Dead specimen",
            status='public', values=(('ACCSTATUS', '0'),)
        )
        self._create_object(
            self.spec_type, title="Living specimen",
            status='public', values=(('ACCSTATUS', '1'),)
        )
        f = forms.SearchForm(data={}, user=None)
        self.assertTrue(f.is_valid())
        res = f.search()
        self.assertNotIn(dead_obj, list(res))
        f = forms.SearchForm(data={}, user=self.user)
        self.assertTrue(f.is_valid())
        res = f.search()
        self.assertIn(dead_obj, list(res))

    def test_search_export(self):
        self._create_object(
            self.spec_type, title="Living specimen",
            status='public', values=(('ACCSTATUS', '1'),)
        )
        response = self.client.get(reverse('search-export') + '?text=living')
        self.assertEqual(
            response['Content-Type'],
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        )
