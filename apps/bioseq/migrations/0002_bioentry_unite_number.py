from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bioseq', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='bioentry',
            name='unite_number',
            field=models.CharField(blank=True, max_length=25),
        ),
    ]
