function open_taxo(url) {
    window.open(url, "taxonomy", "width=600,height=440,resizable=1,scrollbars=1");
    return false;
}

function get_page(lk, url, page_num) {
    /* Get a sublist of content items, paginated */
    $.ajax({
        type: "GET",
        url: url,
        data: "p="+page_num,
        success: function(data) {
            $.cookie(url, page_num);
            lk.parents('.object_block_content').html(data);
        },
        error: function (xhr, ajaxOptions, thrownError){
            alert(xhr.responseText);
        }
    });
    return false;
}

/* Function from object_edit to toggle display of field groups */
function toggle_group(group_name) {
    tbody = $("#"+group_name+"-tbody")
    if (tbody.is (':visible')) {
        tbody.hide();
        $("#"+group_name+"-expander").attr("src", static_url+"img/menu_right.gif");
    } else {
        tbody.show();
        $("#"+group_name+"-expander").attr("src", static_url+"img/menu_down.gif");
    }
    return false;
}

function zoom_image(img_url, width, title) {
    $("#col_func").hide();
    $("#object_content").hide();
    img_html = "\
    <div class='photo' style='width:"+ width +"px;'>\
      <div><img src='" + img_url + "' onclick='return unzoom();'/></div>\
      <div style='float: right;'><a href='#' class='icon' onclick='return unzoom();'><img src='" + static_url + "img/unzoom_icon.png' height='22'/></a></div>\
      <div class='photo_title' style='width:" + width + "px'>" + title + "</div>\
    </div>";
    $("#alt_container").html(img_html);
    $("#alt_container").show();
    return false;
}

function unzoom(img) {
    $("#alt_container").hide();
    $("#object_content").show();
    $("#col_func").show();
    return false;
}

function zoom_table(tbl_url) {
    $.get(tbl_url, function(data) {
        $("#col_func").hide();
        $("#object_content").hide();
        data = "<div style=''><a href='#' class='icon' onclick='return unzoom();'><img src='" + static_url + "img/unzoom_icon.png' height='22'/></a></div>" + data;
        $("#alt_container").html(data);
        $("#alt_container").show();
        $(".tabular").tablesorter();
    });
    return false;
}
