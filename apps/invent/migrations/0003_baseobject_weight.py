from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invent', '0002_baseobject_mgeom'),
    ]

    operations = [
        migrations.AddField(
            model_name='baseobject',
            name='weight',
            field=models.SmallIntegerField(default=0, help_text='Higher numbers will make the item appear first in some lists'),
        ),
    ]
