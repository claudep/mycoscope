from django.db import models
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.utils.translation import gettext_lazy as _

PERM_LIST = (
    ('read', _("Read")),
    ('r+',   _("Read and View private fields")),
    ('rw',   _("Modify")),
)
class Permission(models.Model):
    """ This model link to Groups and has a generic relation to objects
    """
    content_type = models.ForeignKey(ContentType, related_name="permissions", on_delete=models.CASCADE)
    object_id      = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    perm = models.CharField(max_length=10, choices=PERM_LIST)

    def __str__(self):
        return "%s permission on %s for %s" % (self.perm, self.content_object, self.group.name)


class PermissionMixin:
    def has_permission(self, perms, user):
        """ Test if user has at least one of perms permission """
        if user.is_superuser or self.owner == user:
            return True
        elif user.is_anonymous:
            return False
        # Check group perms
        perms = self.permissions.filter(group__in=list(user.groups.all()), perm__in=perms)
        return perms.count() > 0

    def has_read_permission(self, user):
        if self.status == 'public':
            return True
        return self.has_permission(('read', 'rx', 'rw'), user)

    def has_write_permission(self, user):
        if user.is_anonymous:
            return False
        # Check in _perm_cache
        if not hasattr(self, '_perm_cache'): self._perm_cache = {}
        if user.username in self._perm_cache:
            return self._perm_cache[user.username] == 'rw'
        # Not in cache, compute...
        if self.owner == user or user.is_superuser:
            self._perm_cache[user.username] = 'rw'
            return True
        # Check group perms
        write_perms = self.permissions.filter(group__in=list(user.groups.all()), perm='rw')
        if write_perms.count() > 0:
            self._perm_cache[user.username] = 'rw'
            return True
        self._perm_cache[user.username] = ''
        return False

    def add_permission(self, group, perm, apply_to_children=False):
        p, created = Permission.objects.get_or_create(content_type=ContentType.objects.get_for_model(self),
                                                      object_id=self.id, group=group, defaults={'perm':perm})
        if p.perm != perm:
            p.perm = perm
            p.save()
        if apply_to_children:
            for child in self.get_children():
                child.add_permission(group, perm, apply_to_children)

    def add_read_permission(self, group, apply_to_children=False):
        self.add_permission(group, 'read', apply_to_children)

    def add_write_permission(self, group, apply_to_children=False):
        self.add_permission(group, 'rw', apply_to_children)
