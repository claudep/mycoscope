import os

from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import gettext as _

from invent.models import ObjectType, BaseObject, ObjectValue
from invent.tests.base import BaseTestCase

from imports.models import import_file, SavedFile


class ImportTestCase(BaseTestCase):
    fixtures = ['basedata.json']
    # To produce a fresh basedata.json:
    # In local_settings.py, uncomment test DATABASE settings
    # Create the test database (psql -U bcis) : CREATE DATABASE bcis_test OWNER=bcis;
    # Run python manage.py migrate followed by python manage.py load-data
    # Run python manage.py dumpdata invent --indent 2 > apps/invent/fixtures/basedata.json

    def test_import_ods(self):
        file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "odsfile.ods")
        before_len = len(self.inventory)
        otype = ObjectType.objects.get(code="specimen")
        res = import_file(file_path, otype.id, self.user, self.inventory)
        self.assertEqual(res, "2 objects imported on 2 total rows")
        self.assertEqual(before_len + 2, len(self.inventory))
        # Test the INSTCODE_REMARK pseudo attribute is imported as comments
        obj1 = ObjectValue.objects.get(value_text='34-cx').obj
        obj2 = ObjectValue.objects.get(value_text='124.35').obj
        self.assertEqual(obj2.get_value_for('INSTCODE', as_object=True).comments, "Équivalent à AUS006")
        # Test that the imported file has been archived
        self.assertEqual(SavedFile.objects.filter(user=self.user).count(), 1)
        # https://github.com/zbohm/python-ooolib/issues/2
        #self.assertEqual(obj1.get_value_for('REMARKS'), "Contenu\nsur deux lignes")

    def test_update_ods(self):
        # Import first
        file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "odsfile.ods")
        otype = ObjectType.objects.get(code="specimen")
        res = import_file(file_path, otype.id, self.user, self.inventory)
        # Then update
        file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "odsfile-update.ods")
        res = import_file(file_path, otype.id, self.user, self.inventory, update=True)
        # Assertions
        obj1 = ObjectValue.objects.get(value_text='34-cx').obj
        self.assertEqual(obj1.get_value_for('ELEVATION'), 755.5)
        self.assertEqual(obj1.get_value_for('REMARKS'), None)

    def test_import_xlsx(self):
        file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "xlsfile.xlsx")
        before_len = len(self.inventory)
        otype = ObjectType.objects.get(code="specimen")
        res = import_file(file_path, otype.id, self.user, self.inventory)
        self.assertEqual(res, _("%(num1)d objects imported on %(num2)d total rows") % {'num1': 2, 'num2': 2})
        self.assertEqual(before_len + 2, len(self.inventory))
        obj1 = ObjectValue.objects.get(value_text='34-cx').obj
        self.assertEqual(obj1.get_value_for('REMARKS'), "Contenu\nsur deux lignes")
        self.assertEqual(obj1.get_value_for('ATTR_DATE'), '2014-12-23')
        obj2 = ObjectValue.objects.get(value_text='124.35').obj
        self.assertEqual(obj2.get_value_for('ELEVATION'), 600)

    def test_blank_lines(self):
        """ Test file where header and data lines are not respectively 1 and 2 """
        file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "xlsfile_blanks.xlsx")
        before_len = len(self.inventory)
        otype = ObjectType.objects.get(code="specimen")
        res = import_file(file_path, otype.id, self.user, self.inventory, header_line=3, data_first_line=7)
        self.assertEqual(res, "2 objects imported on 2 total rows")
        self.assertEqual(before_len + 2, len(self.inventory))
