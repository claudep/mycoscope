from django.urls import path, re_path
from . import views

urlpatterns = [
    path('<int:obj_id>/import/', views.import_seq_file, name='import_seq_file'),
    path('<int:obj_id>/sequence/<int:seq_id>/', views.display, name='display_sequence'),
    path('<int:obj_id>/sequence/<int:seq_id>/edit/', views.edit_sequence, name='edit_sequence'),
    path('<int:obj_id>/delete/<int:seq_id>/', views.delete_sequence, name='delete_sequence'),
    re_path(r'^(?P<obj_id>\d+)/blast/(?P<seq_id>\d+)/(?P<mode>(local|www))/$', views.blast_sequence, name='blast_sequence'),
    path('<int:obj_id>/blast/<int:blast_id>/display/', views.blast_display, name='blast_display'),
]
